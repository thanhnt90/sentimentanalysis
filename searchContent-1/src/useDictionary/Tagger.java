package useDictionary;

import gate.AnnotationSet;
import gate.Corpus;
import gate.CorpusController;
import gate.Document;
import gate.Factory;
import gate.FeatureMap;
import gate.Gate;
import gate.SimpleAnnotation;
import gate.creole.ANNIEConstants;
import gate.creole.ExecutionException;
import gate.creole.ResourceInstantiationException;
import gate.persist.PersistenceException;
import gate.util.ExtensionFileFilter;
import gate.util.GateException;
import gate.util.OffsetComparator;
import gate.util.Out;
import gate.util.compilers.eclipse.jdt.internal.compiler.parser.Scanner;
import gate.util.persistence.PersistenceManager;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.swing.JPanel;

import org.antlr.grammar.v3.ANTLRv3Parser;
import org.antlr.runtime.Lexer;
import org.antlr.runtime.tree.RewriteRuleTokenStream;
import org.apache.tools.ant.taskdefs.Mkdir;
import org.apache.xerces.dom.CoreDocumentImpl;

import crawler.StringUtils;

import antlr.ANTLRParser;
import antlr.Token;

import net.sourceforge.jeval.EvaluationException;
import net.sourceforge.jeval.Evaluator;

public class Tagger {

	/**
	 * @param args
	 */
	private CorpusController controller;
	private gate.Corpus corpus;
//Load Process From Gapp File
	public CorpusController LoadProcess(String path) throws PersistenceException,
			ResourceInstantiationException, IOException {
		Out.prln("Load Process from Gapp file");
		File file = new File(path);
		controller = (CorpusController) PersistenceManager
				.loadObjectFromFile(file);
		return controller;
	}
//Set Corpus
	public void setCorpus(String pathCorpus)
			throws ResourceInstantiationException, IOException {
		corpus = Factory.newCorpus("CorpusForTag");
		File directoryCorpus = new File(pathCorpus);
		ExtensionFileFilter filter = new ExtensionFileFilter("TXT File", "txt");
		URL url = directoryCorpus.toURI().toURL();
		corpus.populate(url, filter, "UTF-8", true);
		System.out.println("\n" + corpus.getDocumentNames());
		controller.setCorpus(corpus);
		Out.prln("Corpus Loaded....");
		Out.prln("-----"+corpus.size()+"  Documents had been Loaded....");
			}
	//convert List to Document and add to New Corpus;
	public void converListToDoc(ArrayList<String> arr) throws ResourceInstantiationException{
		corpus = Factory.newCorpus("CorpusForTag");
		//corpus = (Corpus) new CoreDocumentImpl();
			for(int i = 0 ; i<arr.size();i++){
			Document doc = Factory.newDocument(arr.get(i).toString());
			System.out.println(doc.getContent());
			corpus.add(doc);
		}
		controller.setCorpus(corpus);
		}
// Get Annotation For Each Sentence
	public List<String[]> GetAnnotationForEachDoc(List<String> sentences) {
		List<String[]> result = new ArrayList<String[]>();
		Iterator iter = corpus.iterator();
		int check = 0;
		while (iter.hasNext()) {
			Document doc = (Document) iter.next();
			AnnotationSet annSet = doc.getAnnotations();
			
			//==============CHECK FOR ANNOTING============
			AnnotationSet checkToken = annSet
					.get(ANNIEConstants.TOKEN_ANNOTATION_TYPE);
			if(checkToken.size() == 0){
				System.err.println("ERROR IN : "+doc.getName()+"None Token Annotation");
				check++;
				
			}
			else{check=0;}
			//===========================================================================
			AnnotationSet Sentences = annSet
					.get(ANNIEConstants.SENTENCE_ANNOTATION_TYPE);
			ArrayList annListSentence = new ArrayList(Sentences);
			Collections.sort(annListSentence, new OffsetComparator());
			List<String> tokenList = new ArrayList<String>();
			StringBuilder sentence = new StringBuilder();
			for (int i = 0; i < annListSentence.size(); i++) {
				Map<Long, FeatureMap> map = new HashMap<Long, FeatureMap>();
				AnnotationSet mentions = annSet.get("Mention",
						((SimpleAnnotation) annListSentence.get(i))
								.getStartNode().getOffset(),
						((SimpleAnnotation) annListSentence.get(i))
								.getEndNode().getOffset());
				for (gate.Annotation token : mentions) {
					long id = token.getStartNode().getOffset();
					map.put(id, token.getFeatures());
				}

				Map<Long, String> tokenMap = new TreeMap<Long, String>();
				Map<Long, String> categories = new HashMap<Long, String>();
				AnnotationSet tokens = annSet.get(
						ANNIEConstants.TOKEN_ANNOTATION_TYPE,
						((SimpleAnnotation) annListSentence.get(i))
								.getStartNode().getOffset(),
						((SimpleAnnotation) annListSentence.get(i))
								.getEndNode().getOffset());

				for (gate.Annotation token : tokens) {
					long id = token.getStartNode().getOffset();
					String string = (String) token.getFeatures().get("string");
					String category = (String) token.getFeatures().get(
							"category");
					tokenMap.put(id, string);
					categories.put(id, category);
				}

				/*for (Object token : tokenMap.values()) {
					System.out.print(" " + token);
				}*/
				//System.out.println();

				for (Entry token : tokenMap.entrySet()) {
					String tokenString = (String) token.getValue();
					sentence.append("_").append(tokenString);
					long id = (Long) token.getKey();
					FeatureMap featureMap = map.get(id);
					String sentiment = "";
					if (featureMap != null) {
						for (Entry entry : featureMap.entrySet()) {
							sentiment = (String) entry.getValue();
							continue;
						}
					}
					tokenList.add(sentiment + "_" + categories.get(id));
				}
				
			}
			sentences.add(sentence.toString());
			String[] tokenArray = new String[tokenList.size()];
			result.add(tokenList.toArray(tokenArray));
		}
		
		
		//////////////////////
		if(check==0){System.out.println("ALL DOCUMENTS HAD BEEN TAGGED!");}
		///////////////////////
		
		
		return result;
	}

	public void runProcess()  {
       		try {
				controller.execute();
				} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		Out.prln("Running Process.......................");
    
	}

	public boolean checkSentiment(String s) {
		if (s.contains("Positive") || s.contains("Negative"))
			return true;
		else
			return false;
	}

	// Tính toán trọng số của 1 câu
	public String convertToComperator(String[] listAnnotationEachSentence) {
		// String[] result = new String[100];
		StringBuilder stringbuilder = new StringBuilder();
		// / Nếu từ đầu là Positive hay Negative , trọng số cao hơn
		if (listAnnotationEachSentence[0].contains("Positive"))
			stringbuilder.insert(0, "5+");
		if (listAnnotationEachSentence[0].contains("Negative"))
			stringbuilder.insert(0, "(-5)+");
		Boolean flag7 = false;
		Boolean flag4 = false;
		Boolean flag5 = false;
		Boolean flag6 = false;
		for (int i = 0; i < listAnnotationEachSentence.length; i++) {

			// -- nếu từ Positive có từ loại không xác định - trọng số =0
			if (listAnnotationEachSentence[i].startsWith("Positive")) {
				if (listAnnotationEachSentence[i].equals("Positive_X")) {
					stringbuilder.append("0+");
				}
				else{	if (flag7 == true) {
					stringbuilder.append("(-5)*");
					flag7 = false;
				}
				     if(flag4){
				    	 stringbuilder.append("10*");
				    	 flag4 =false;
				     }
				     if(flag5){
				    	 stringbuilder.append("2*");
				    	 flag5 =false;
				     }
				     if(flag6){
				    	 stringbuilder.append("1.25*");
				    	 flag6 =false;
				     }
					stringbuilder.append("1+");
				}
				
			} else if (listAnnotationEachSentence[i].toString().contains(
					"Negative")) {
				if (flag7 == true) {
					stringbuilder.append("(-5)*");
					flag7 = false;
				}
				   if(flag4){
				    	 stringbuilder.append("10*");
				    	 flag4 =false;
				     }
				     if(flag5){
				    	 stringbuilder.append("2*");
				    	 flag5 =false;
				     }
				     if(flag6){
				    	 stringbuilder.append("1.25*");
				    	 flag6 =false;
				     }
				stringbuilder.append("(-1)+");
			// Trọng số của SpecialKind5 là x2 trọng số của từ đứng sau
			} else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind5")) {
				flag5=true;
				//stringbuilder.append("2*");
			// /// Trọng số của SpecialKind4 là x10 trọng số của từ đứng sau
			} else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind4")) {
				flag4 = true;
				//stringbuilder.append("10*");
			} else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind6")) {
				//stringbuilder.append("1.25*");
				flag6 = true;
			// ///////////////////////////////////////////
			} else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind7")) {
				flag7 = true;
			} else if (flag7 == true && (listAnnotationEachSentence[i].contains("_,")
					|| listAnnotationEachSentence[i].equals("_C"))) {
				flag7 = false;
			} else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind8"))
				stringbuilder.append("");
			// //////////////////////////////////////////////////////////////////////////
			else if (listAnnotationEachSentence[i].toString().contains(
					"SpecialKind2_2")) {
				stringbuilder.append("0)*0+");
				stringbuilder.insert(0, "(");
			}
		}
		stringbuilder.append("0");
		
		return stringbuilder.toString();
	}
	
	
	public Float[] returnValueOfDocument() throws EvaluationException{
		List<String> originalSentences = new ArrayList<String>();
		List<String[]> sentences = GetAnnotationForEachDoc(originalSentences);
		//System.err.println(sentences.size()+"Sentences");
		int count = 0;
		Float[] result = new Float[sentences.size()];
		for (int i = 0; i < sentences.size(); ++i) {
			count++;
			String[] sentence = sentences.get(i);
			String formula = convertToComperator(sentence);
			String s = new Evaluator().evaluate(formula);
			result[i] = Float.parseFloat(s);
		}
		return result ;
	}
	
	public static ArrayList<String> classifierUsingDictionary(ArrayList<String> listSentences, String keyWorlds)throws GateException, IOException, EvaluationException {
		ArrayList<String> result = new ArrayList<String>();
		if(Gate.getPluginsHome() == null)
			Gate.setPluginsHome(new File("//Users//ThanhNT//Desktop//OMSA"));
		Gate.init();
		Tagger tagger = new Tagger();
		String pathProcess = "/Users/ThanhNT/Desktop/OMSA/TEST2";
		tagger.LoadProcess(pathProcess);
		tagger.converListToDoc(listSentences);
		tagger.runProcess();
		Float[] valueEachDocs= tagger.returnValueOfDocument();

		int i = 0;
		String path = "//Users//ThanhNT//Desktop//LogForUseDictionary" + System.getProperty("file.separator") + keyWorlds + System.getProperty("file.separator");
		File outputFolder = new File(path);
		if (!outputFolder.exists()) {
			outputFolder.mkdirs();
		} else {
			for(File file: outputFolder.listFiles()) {
				file.delete();
			}
		}
		System.out.println("Path :" + path);
		
		File folderPos = new File(outputFolder, "pos");
		folderPos.mkdir();
		File folderNeg = new File(outputFolder, "neg");
		folderNeg.mkdir();
		File folderUnknow = new File(outputFolder, "unknow");
		folderUnknow.mkdir();
		for(float value:valueEachDocs){
			if(value > 0){
				result.add("pos");
				StringUtils.toFile(listSentences.get(i), new File(folderPos, Integer.toString(i)));
			}
			else
				if(value ==0 ){
					result.add("unknow");
					StringUtils.toFile(listSentences.get(i), new File(folderUnknow, Integer.toString(i)));
				}
				else{
					result.add("neg");
					StringUtils.toFile(listSentences.get(i), new File(folderNeg, Integer.toString(i)));
				}
			i++;
		}

		return result;
	}
	
	public static void main(String[] args) throws GateException, IOException, EvaluationException {
		//Gate.setGateHome(new File("C:/Program Files/GATE_Developer_7.0"));
		Gate.setPluginsHome(new File("//Users//ThanhNT//Desktop//OMSA"));
		Gate.init();
		Tagger tagger = new Tagger();
		String pathProcess = "/Users/ThanhNT/Desktop/OMSA/TEST2";
		String pathCorpus = "C:/Users/ThanhTung/Desktop/My GATE/DataTraining/Nokia/test";
		tagger.LoadProcess(pathProcess);
		ArrayList<String> arr = new ArrayList<String>();
		arr.add("thích dáng nokia nhất.");
		arr.add("không thích dáng nokia");
		tagger.converListToDoc(arr);
		//tagger.setCorpus(pathCorpus);
		tagger.runProcess();
		Float[] valueEachDocs= tagger.returnValueOfDocument();
		/*for(Float stringValue:valueEachDocs){
			System.out.println(stringValue);
		}*/
		//System.err.println(valueEachDocs);
		/*
		List<String> originalSentences = new ArrayList<String>();
		List<String[]> sentences = tagger.GetAnnotationForEachDoc(originalSentences);
		//System.err.println(sentences.size()+"Sentences");
		int count = 0;
		/* for (int i = 0; i < sentences.size(); ++i) {
			count++;
			System.out.println(count);
			String[] sentence = sentences.get(i);
			String formula = tagger.convertToComperator(sentence);
		
			try {
				String s = new Evaluator().evaluate(formula);
				// System.err.println(result);
				//if (result > 0) {
					//cout++;
			//	} else {
					System.out.println(originalSentences.get(i));
					for (int j = 0; j < sentence.length; ++j) {
						System.out.print(" " + sentence[j]);
					}
					System.out.println("###########");
					System.out.println(formula+" = "+s);
					System.out.println(s);
					System.out.println("###########");
				}
				// System.err.println(cout);
			 catch (EvaluationException e) {
				e.printStackTrace();
			}
			
		}*/
	}

}
