package indexAndSearch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import crawler.StringUtils;

public class TextIndexEngine {
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");
	public static final String FIELD_PATH = "path";
	public static final String FIELD_CONTENT = "content";

	 

	public static void indexData(File folderData, File folderIndexed)throws CorruptIndexException, LockObtainFailedException, IOException{
		if(!folderIndexed.isDirectory()){
			folderIndexed.mkdir();
		}
		IndexWriter writer = null;
		FSDirectory dir;
		try {
			dir = FSDirectory.open(folderIndexed);
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_35);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35, analyzer);
			writer = new IndexWriter(dir, config);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		int originalNumDocs = writer.numDocs();

		if(!folderData.isDirectory()){
			Document doc = new Document();
			doc.add(new Field(FIELD_CONTENT, StringUtils.fromFile(folderData), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
			doc.add(new Field(FIELD_PATH, folderData.getPath(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));
			writer.addDocument(doc);
		}
		else{
			for(File file:getSubfile(folderData)){
				Document doc = new Document();
				doc.add(new Field(FIELD_CONTENT, StringUtils.fromFile(file), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
				doc.add(new Field(FIELD_PATH, file.getPath(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS));
				writer.addDocument(doc);
			}
		}
		int newNumDocs = writer.numDocs();
		System.out.println("");
		System.out.println("************************");
		System.out.println((newNumDocs - originalNumDocs) + " documents added.");
		System.out.println("************************");
		writer.close();
		
	}
	
	public static File[] getSubfile(File folder) {
		List<File> subfolders = new ArrayList<File>();
		if(folder.isDirectory()){
			for(File file: (folder).listFiles()){
				File[] files = getSubfile(file);
				for(File subfolder: files) {
					subfolders.add(subfolder);	
				}
			}
		}
		else
			subfolders.add(folder);
		File[] result = new File[subfolders.size()];
		return subfolders.toArray(result);
	}
	
	public static void main(String[] arg){
		try {
			indexData(new File("//Users//ThanhNT//Desktop//trainData//trainAll"), new File("//Users//ThanhNT//Desktop//indexTrainData"));
		} catch (CorruptIndexException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LockObtainFailedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
