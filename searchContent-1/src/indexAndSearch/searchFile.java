package indexAndSearch;

import java.io.File;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import crawler.StringUtils;
import crawler.TextFileIndexEngine;

public class searchFile {
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");
	public static final String RESULT_DIRECTORY = "//Users//ThanhNT//Desktop//demo_result";
	public static final int HITS_PER_PAGE = 1000000;
	public static final String[] FIELDS_TO_SEARCH = {TextIndexEngine.FIELD_CONTENT};

	public static void main(String[] args) {
		searchStringInFolder("điện thoại nữ", 
				"//Users//ThanhNT//Desktop//indexTrainData");
	}
	
	public static void searchStringInFolder (String needSearch, String indexFolderPath){
		try {
			FSDirectory directory = FSDirectory.open(new File(
					indexFolderPath));
			IndexSearcher indexSearcher = new IndexSearcher(
					IndexReader.open(directory));
			Query query = queryForPhrase(FIELDS_TO_SEARCH, needSearch);

			TopScoreDocCollector collector = TopScoreDocCollector.create(HITS_PER_PAGE, true);
			indexSearcher.search(query, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			
			for(int i = 0; i < hits.length; i++){
				int docId = hits[i].doc;
				Document doc = indexSearcher.doc(docId);
				String text = doc.get(TextIndexEngine.FIELD_CONTENT);
				System.out.println( hits[i].score + text + doc.get(TextIndexEngine.FIELD_PATH));
//				indexSearcher.getIndexReader().close();
//				indexSearcher.close();
//				return text;
					
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
//		return "";
	}
	
	public static void convertSentenceToParagrap (File sourceFolder, File destinFolder){
		int fileNameCount = 0;

		for(File f: sourceFolder.listFiles()){
			if(f.getName().charAt(0) == '.') continue;
			String sentence = StringUtils.fromFile(f);
//			sentence = "đàm vĩnh hưng";
			try {
				FSDirectory directory = FSDirectory.open(new File(
						TextFileIndexEngine.INDEX_DIRECTORY));
				IndexSearcher indexSearcher = new IndexSearcher(
						IndexReader.open(directory));
//				System.out.println("text:----------->" + sentence);
				Query query = queryForPhrase(FIELDS_TO_SEARCH, sentence);

				TopScoreDocCollector collector = TopScoreDocCollector.create(HITS_PER_PAGE, true);
				indexSearcher.search(query, collector);
				ScoreDoc[] hits = collector.topDocs().scoreDocs;
				System.out.println("file name: " + f.getName() + "  number result: " + hits.length);
				fileNameCount++;

				for (int i = 0; i < hits.length; ++i) {
					int docId = hits[i].doc;
					Document doc = indexSearcher.doc(docId);
					String text = doc.get(TextFileIndexEngine.FIELD_COMMENT);
					if(hits.length > 1)
						System.out.println("result: " + fileNameCount);
					StringUtils.toFile(text, new File(destinFolder.getPath() + FILE_SEPARATOR + fileNameCount + "_" + i + ".txt"));
				}
				if(hits.length == 0){
					fileNameCount++;
					StringUtils.toFile(sentence, new File(destinFolder.getPath() + FILE_SEPARATOR + fileNameCount + ".txt"));
				}
				indexSearcher.getIndexReader().close();
				indexSearcher.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static Query queryForPhrase(String[] fields, String phrase) {
		String[] keyWords = phrase.split(" ");
		BooleanQuery query = new BooleanQuery();
		for (String field : fields) {
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_35);

			QueryParser queryParser = new QueryParser(Version.LUCENE_35, field, analyzer);

			queryParser.setDefaultOperator(QueryParser.Operator.AND);

			Query phraseQuery;
			try {
				phraseQuery = queryParser.parse(QueryParser.escape(phrase));
				query.add(phraseQuery, Occur.SHOULD);

			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			PhraseQuery phraseQuery = new PhraseQuery();
//			for (String keyWord : keyWords) {
//				if(keyWord.length() < 2) continue;
//				
//				Term term = new Term(field, keyWord);
//				phraseQuery.add(term);
//				phraseQuery.setSlop(0);
//			}
		}
		return query;
	}
}
