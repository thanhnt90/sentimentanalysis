package stograge;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import crawler.Article;
import crawler.Post;
import crawler.StringUtils;
import crawler.StringUtils.ReturnCode;

public class FileStograge {
	private static String METADATA_SEPARATOR = "\b";
	static class Metadata {
		private String fileName;
		private List<String> rows;
		
		public Metadata(String fileName) {
			this.fileName = fileName;
			rows = new ArrayList<String>();
		}
		
		public void addRow(String row) {
			rows.add(row);
		}
		
		public String getRow(int i) {
			return rows.get(i);
		}
		
		public String getFileName() {
			return fileName;
		}
		
		@Override
		public String toString() {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(fileName).append(METADATA_SEPARATOR);
			for(String row: rows) {
				stringBuilder.append(row).append(METADATA_SEPARATOR);
			}
			return stringBuilder.toString();
		}
	}
	
	private static String FILE_SEPARATOR = System.getProperty("file.separator");
	public static byte FILE_NAME_LENGHT = 4;
	public static Pattern POST_NAME_PATTERN = Pattern.compile("\\d{" + FILE_NAME_LENGHT + "}");
	public static String METADATA_FILE = "meta";
	private static final DateFormat DATE_FORMAT
		= new SimpleDateFormat("hh:mm|dd/MM/yyyy");

	private static final DateFormat DATE_FORMAT_FILENAME = new SimpleDateFormat("yyMMdd");
	
	private File dataDirectory;

	public FileStograge(String directory) {
		this.dataDirectory = new File(directory);
	}

	public File getDataDirectory() {
		return dataDirectory;
	}

	public void setDataDirectory(File dataDirectory) {
		this.dataDirectory = dataDirectory;
	}

	public ReturnCode saveOrUpdate(Article article) {
		String fileName = DATE_FORMAT_FILENAME.format(article.getDate()) + article.getId();
		File directory = new File(dataDirectory, fileName);
		directory.mkdirs();
		
		StringBuilder metaString = new StringBuilder();
		
		Metadata metadata = new Metadata(fileName);
		metadata.addRow("" + article.getId());
		metadata.addRow(DATE_FORMAT.format(article.getDate()));
		metadata.addRow(article.getTitle());
		metadata.addRow(article.getLead());
		metadata.addRow("" + article.getLikes());
		metaString.append(metadata);
		
		ReturnCode saveStatus = StringUtils.toFile(article.getContent(), new File(directory, fileName));
		if(saveStatus == ReturnCode.FAILED) return ReturnCode.FAILED;
		
		List<Post> comments = article.getComments();
		for(int i = 0; i<comments.size(); ++i) {
			Post post = comments.get(i);
			fileName = DATE_FORMAT_FILENAME.format(post.getDate()) + article.getId();
			fileName += String.format("%0" + FILE_NAME_LENGHT + "d", i+1);
			metadata = new Metadata(fileName);
			metadata.addRow(DATE_FORMAT.format(post.getDate()));
			metadata.addRow(post.getTitle());
			metadata.addRow("" + post.getLikes());
			metaString.append(metadata);
			StringUtils.toFile(post.getContent(), new File(directory, fileName));
		}
		saveStatus = StringUtils.toFile(metaString.toString(), new File(directory, METADATA_FILE));
		return saveStatus;
	}
	
	public Metadata metadataFromRows(String[] rows, int offset, int nRows) {
		String fileName = rows[offset];
		Metadata metadata = new Metadata(fileName);
		for(int i = 0; i<nRows; ++i) {
			metadata.addRow(rows[++offset]);
		}
		return metadata;
	}
	
	public Article articleFromMetadata(Metadata metadata) {
 		Article article = new Article();
 		int i = 0;
		try {
			String token = metadata.getRow(i++);
			article.setId(Long.parseLong(token));
			token = metadata.getRow(i++);
			article.setDate(DATE_FORMAT.parse(token));
			token = metadata.getRow(i++);
			article.setTitle(token);
			token = metadata.getRow(i++);
			article.setLead(token);
			token = metadata.getRow(i++);
			article.setLikes(Integer.parseInt(token));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return article;
	}
	
	public Post commentFromMetadata(Metadata metadata) {
 		Post post = new Post();
 		int i = 0;
		try {
			String token = metadata.getRow(i++);
			post.setDate(DATE_FORMAT.parse(token));
			token = metadata.getRow(i++);
			post.setTitle(token);
			token = metadata.getRow(i++);
			post.setLikes(Integer.parseInt(token));
			post.setFileName(metadata.fileName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return post;
	}
	
	public Article articleFromDirectory(String thread) {
		String path = dataDirectory.getAbsoluteFile() + FILE_SEPARATOR + thread + FILE_SEPARATOR;
		String meta = StringUtils.fromFile(new File(path + METADATA_FILE));
		String[] rows = meta.split(METADATA_SEPARATOR);

		Metadata metadata = metadataFromRows(rows, 0, 5);
		Article article = articleFromMetadata(metadata);
		String content = StringUtils.fromFile(new File(path + metadata.getFileName()));
		article.setContent(content);
		int offset = 6;
		while (offset < rows.length) {
			metadata = metadataFromRows(rows, offset, 3);
			Post post = commentFromMetadata(metadata);
			content = StringUtils.fromFile(new File(path + metadata.getFileName()));
			post.setContent(content);
			article.getComments().add(post);
			offset += 4;
		}
		
		return article;
	}

	public String[] listThreads() {
		return dataDirectory.list();
	}
}
