package lusenceSearch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import crawler.StringUtils;
import crawler.TextFileIndexEngine;

public class test {
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");
	public static final String RESULT_DIRECTORY = "//Users//ThanhNT//Desktop//result_tech";
	public static final Pattern KEYWORD_EXTRACT_PATTERN = Pattern
			.compile("\"([^\"]*)\"");

	public static final String[] FIELDS_TO_SEARCH = {
	/*
	 * TextFileIndexEngine.FIELD_LEAD, TextFileIndexEngine.FIELD_TITLE,
	 */
	TextFileIndexEngine.FIELD_CONTENT};

	public static final int HITS_PER_PAGE = 1000000;

	public static void main(String[] args) {
		//
//		String[] keyWords = {"người hùng", "yêu", "đánh giá cao"};
//		String[] keyWords = {"tuyệt hảo", "ngon", "hấp dẫn"};
//		String[] keyWords = {"lý tưởng", "rẻ", "good", "chất lượng cao", "ổn định"};
//		String[] keyWords = {"hâm mộ", "ngưỡng mộ", "sẽ mua", "trung thành"};
//		String[] keyWords = {"ưng ý", "hài lòng", "mượt", "sạch"};
//		String[] keyWords = {"hưởng ứng", "lựa chọn tốt"};
//		String[] keyWords = {"hoàn hảo", "pro", "nhanh", "tinh tế", "đẹp"};
		String[] keyWords = {"dễ dùng", "dễ sử dụng"};
		for (String keyword : keyWords) {
			List<String> keywords = new ArrayList<String>();
			keywords.add(keyword);
//			keywords.add("tiện dụng");
//			keywords.add("thu hút");
//			keywords.add("hài lòng");
//			keywords.add("thoả mãn");
//			keywords.add("thành công");


			//keywords.add("idol");



			try {
				FSDirectory directory = FSDirectory.open(new File(
						TextFileIndexEngine.INDEX_DIRECTORY));
				IndexSearcher indexSearcher = new IndexSearcher(
						IndexReader.open(directory));

				Query query = queryForMultiphrase(keywords, "080101", "120714");

//				Query query = new Query() {
//					
//					/**
//					 * 
//					 */
//					private static final long serialVersionUID = 1L;
//
//					@Override
//					public String toString(String arg0) {
//						// TODO Auto-generated method stub
//						return FIELDS_TO_SEARCH[0]+":" + "xe bus"+ " OR " + FIELDS_TO_SEARCH[0] + ":" + "xe buýt";
//					}
//				};
				TopScoreDocCollector collector = TopScoreDocCollector.create(
						HITS_PER_PAGE, true);

				indexSearcher.search(query, collector);

				ScoreDoc[] hits = collector.topDocs().scoreDocs;

				saveResult(hits, indexSearcher, query);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Complete!");
	}

	public static Query queryForPhrase(String[] fields, String phrase) {
		String[] keyWords = phrase.split(" ");

		BooleanQuery query = new BooleanQuery();
		for (String field : fields) {
			PhraseQuery phraseQuery = new PhraseQuery();
			for (String keyWord : keyWords) {
				Term term = new Term(field, keyWord);
				phraseQuery.add(term);
				phraseQuery.setSlop(0);
			}
			query.add(phraseQuery, Occur.SHOULD);
		}
		return query;
	}

	public static Query queryForMultiphrase(List<String> phrases,
			String startDate, String endDate) {
		BooleanQuery booleanQuery = new BooleanQuery();
		for(String phrase : phrases){
			BooleanQuery boolPhrase = new BooleanQuery();
			boolPhrase.add(queryForPhrase(FIELDS_TO_SEARCH, phrase), Occur.MUST);
//			boolPhrase.add(new TermRangeQuery(TextFileIndexEngine.FIELD_DATE, startDate, endDate, true, true), Occur.MUST);
			booleanQuery.add(boolPhrase, Occur.SHOULD);

		}
//		for (String phrase : phrases) {
//			booleanQuery.add(queryForPhrase(FIELDS_TO_SEARCH, phrase),
//					Occur.MUST);
//		}
//		booleanQuery.add(new TermRangeQuery(TextFileIndexEngine.FIELD_DATE,
//				startDate, endDate, true, true), Occur.MUST);
		return booleanQuery;
	}

	public static void saveResult(ScoreDoc[] hits, IndexSearcher indexSearcher,
			Query query) throws CorruptIndexException, IOException {
		String queryString = query.toString();
		System.out.println(queryString + "\n" + hits.length + " hit(s):");
		Matcher matcher = KEYWORD_EXTRACT_PATTERN.matcher(queryString);

		queryString = queryString.replace("+", "").replace("[", "")
				.replace("]", "").replace(":", "").replace(" ", "")
				.replace("(", "").replace(")", "");
		int queryLength = queryString.length();
//		queryString = queryString.substring(queryLength - 14, queryLength);

		Set<String> keywords = new HashSet<String>();
		int start = 0;
		while (matcher.find(start)) {
			keywords.add(matcher.group(1));
			start = matcher.toMatchResult().end(0);
		}
		String[] keyWords = new String[keywords.size()];
		keywords.toArray(keyWords);

		queryString = "";
		for (String keyword : keyWords) {
			queryString = keyword + "_" + queryString;
		}

		String path = RESULT_DIRECTORY + FILE_SEPARATOR + queryString;

		StringBuilder stringBuilder = new StringBuilder();

		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document doc = indexSearcher.doc(docId);
			String text = doc.get(TextFileIndexEngine.FIELD_PATH);
			//System.out.println(text);
			text = doc.get(TextFileIndexEngine.FIELD_CONTENT);
			List<String> hightlighted = getHightlighted(text, keyWords);

			for (String string : hightlighted) {
				stringBuilder.append(string).append("\r\n\r\n");
			}
		}

		StringUtils.toFile(stringBuilder.toString(), new File(path));
	}

	public static List<String> getHightlighted(String content, String[] keyWords) {

		List<String> sentences = new ArrayList<String>();

		Pattern pattern = Pattern.compile("[^…\\n\\.!?]+[…\\n\\.!?]+");
		Matcher matcher = pattern.matcher(content);
		int start = 0;
		while (matcher.find(start)) {
			String sentence = matcher.group(0).trim();
			if (sentence.length() > 0) {
				sentences.add(sentence);
			}
			start = matcher.toMatchResult().end(0);
		}

		List<String> result = new ArrayList<String>();

		Set<Integer> hits = new TreeSet<Integer>();

		for (int i = 0; i < sentences.size(); i++) {
			for (String keyWord : keyWords) {
				if (sentences.get(i).toLowerCase()
						.indexOf(keyWord.toLowerCase()) > -1) {
					if (i > 0) {
						hits.add(i - 1);
					}
					hits.add(i);
					if (i < (sentences.size() - 1)) {
						hits.add(i + 1);
					}
				}
			}
		}

		StringBuffer hightlighted = new StringBuffer();

		Iterator<Integer> hit = hits.iterator();
		Integer prev = null;
		while (hit.hasNext()) {
			int current = hit.next();
			if (prev != null && current - prev > 1) {
				result.add(hightlighted.toString());
				hightlighted.setLength(0);
			}
			prev = current;
			hightlighted.append(sentences.get(current)).append(' ');
		}

		if (hightlighted.length() > 0)
			result.add(hightlighted.toString());
		return result;
	}
}