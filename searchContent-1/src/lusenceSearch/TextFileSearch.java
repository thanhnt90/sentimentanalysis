package lusenceSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.DefaultSimilarity;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Similarity;
import org.apache.lucene.search.TermRangeQuery;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;

import Entities.postTested;

import crawler.StringUtils;
import crawler.TextFileIndexEngine;

public class TextFileSearch {
	public static final boolean USESENTENCE = false;
	public static final String FILE_SEPARATOR = System
			.getProperty("file.separator");
	public static final String RESULT_DIRECTORY = "//Users//ThanhNT//Desktop//demo_result";
	public static final Pattern KEYWORD_EXTRACT_PATTERN = Pattern
			.compile("\"([^\"]*)\"");

	public static final String[] FIELDS_TO_SEARCH = {
		TextFileIndexEngine.FIELD_COMMENT};

	public static final int HITS_PER_PAGE = 1000000;
	public String pathResultFolder;
	private ArrayList<postTested> resultSearch;

	public ArrayList<postTested> getResultSearch() {
		return resultSearch;
	}

	public void setResultSearch(ArrayList<postTested> resultSearch) {
		this.resultSearch = resultSearch;
	}

	public static void main(String[] args) {

		String content = "thể, thuyết phục, nổi tiếng, mệnh lệnh, đàm phán, thực sự, mạnh mẽ, thắc mắc, niềm say mê, phù hợp, điều chỉnh, khẳng định, dễ thương, phát sinh, bóng, vui tươi, cẩn thận, sạch sẽ, khen thưởng, nhất quán, chữa bệnh, mong muốn, nghiêm túc, giải trí, tâm trạng phấn khởi, linh hoạt, vui tươi, hài hòa tốt, khiêm tốn, sáng tạo trò đùa, may mắn, có ý nghĩa, trọng, tối ưu, hòa bình, lợi nhuận đề phòng, có mục đích, tôn giáo, phong phú, đáp ứng, chân thành, trang nghiêm, ngọt, hiệp ước, ngay thẳng, sức sống, dí dỏm, woo, tay nghề, giá trị, đỉnh cao, tạm ứng, làm giảm bớt, vỗ tay, gia tăng, từ tâm, sự sáng chói, vui vẻ, hợp tác toàn diện, ấm cúng, dành, đặc biệt, chứng thực, chính xác, nhiệt tình, bồi dưỡng, năng khiếu, thỏa mãn, anh hùng, không thể thiếu, quan tâm, kiến thức, hùng vĩ, lưới, nuôi dưỡng, nguy nga, đánh bóng, chủ yếu là, thịnh vượng, tinh chỉnh, cứu nạn, an toàn, lành mạnh, nhạy cảm, phù hợp với, phát triển mạnh, bao la, dễ hiểu, khôn ngoan, tuyệt vời, khả thi, nổi tiếng thế giới, xứng đáng";
		//		String[] keyWords = content.split(", ");	
		String[] keyWords = {"đàm vĩnh hưng"};
		for (String keyword : keyWords) {
			List<String> keywords = new ArrayList<String>();
			keywords.add(keyword);
			try {
				FSDirectory directory = FSDirectory.open(new File(
						TextFileIndexEngine.INDEX_DIRECTORY));
				IndexReader ir = IndexReader.open(directory);

				IndexSearcher indexSearcher = new IndexSearcher(ir);

//				TermDocs termDocs = indexSearcher.getIndexReader().termDocs();
//				TermEnum termEnum = indexSearcher.getIndexReader().terms();
//
//				while (termDocs.next()){
//					int docId = termDocs.doc();
//					int freq = termDocs.freq();
//					//		          System.out.println("Doc Id: " + docId + " freq " + freq);
//				}
//				while (termEnum.next()){
//					Term term = termEnum.term();
//					int freq1 = termEnum.docFreq();
//					System.out.println("term: " + term + " freq " + freq1);
//				} 

				Similarity sim = new DefaultSimilarity(){
					
					@Override
					public float tf(float freq) {
						if(freq > 2)
							return super.tf(freq);
						System.out.println(">>>>>>>> freq = " + freq);
						
						return 0;
					}
					
					@Override
				    public float idf(int docFreq, int numDocs) {
				        return super.idf(docFreq, numDocs);
				    }
				};
				Query query = queryForMultiphrase(keywords, "080101", "120714");

				TopScoreDocCollector collector = TopScoreDocCollector.create(
						HITS_PER_PAGE, true);
//				indexSearcher.setSimilarity(sim);
				indexSearcher.search(query, collector);
				
				

				ScoreDoc[] hits = collector.topDocs().scoreDocs;
				System.out.println("number result: " + hits.length);
				for (int i = 0; i < hits.length; ++i) {
					int docId = hits[i].doc;
					Document doc = indexSearcher.doc(docId);
					
					String text = doc.get(TextFileIndexEngine.FIELD_COMMENT);
					String afterSplit[] = text.split(keywords.get(0));
					System.out.println("number of match: " + afterSplit.length);
					System.out.println(text);
					double score = hits[i].score;
					System.out.println("----------->" + score);

					//text = StringUtils.fromFile(new File(text));
					
				}
				
				//		          TermFreqVector vector = indexSearcher.getIndexReader().getTermFreqVector(docId, "field");

				/*
				Query query = queryForMultiphrase(keywords, "080101", "120714");

				TopScoreDocCollector collector = TopScoreDocCollector.create(
						HITS_PER_PAGE, true);

				indexSearcher.search(query, collector);

				ScoreDoc[] hits = collector.topDocs().scoreDocs;

				saveResult(hits, indexSearcher, query);
				 */
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Complete!");
	}

	public long search(List<String> keyWords, String startDate, String endDate){
		if(resultSearch == null)
			resultSearch = new ArrayList<postTested>();
		else
			resultSearch.clear();
		
		List<String> keywords = new ArrayList<String>();
		for (String keyword : keyWords) {
			keywords.add(keyword);
		}

		try {
			FSDirectory directory = FSDirectory.open(new File(
					TextFileIndexEngine.INDEX_DIRECTORY));
			IndexSearcher indexSearcher = new IndexSearcher(
					IndexReader.open(directory));
			Query query = queryForMultiphrase(keywords, startDate, endDate);
			TopScoreDocCollector collector = TopScoreDocCollector.create(
					HITS_PER_PAGE, true);
			indexSearcher.search(query, collector);
			ScoreDoc[] hits = collector.topDocs().scoreDocs;
			pathResultFolder = saveResult1(hits, indexSearcher, query);
			System.out.println("path file: " + pathResultFolder);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return (new File(pathResultFolder)).listFiles().length;
	}

	public static Query queryForPhrase(String[] fields, String phrase) {
		String[] keyWords = phrase.split(" ");
		BooleanQuery query = new BooleanQuery();
		for (String field : fields) {
			PhraseQuery phraseQuery = new PhraseQuery();
			for (String keyWord : keyWords) {
				Term term = new Term(field, keyWord);
				phraseQuery.add(term);
				phraseQuery.setSlop(0);
			}
			query.add(phraseQuery, Occur.SHOULD);
		}
		return query;
	}

	public static Query queryForMultiphrase(List<String> phrases,
			String startDate, String endDate) {
		BooleanQuery booleanQuery = new BooleanQuery();
		for(String phrase : phrases){
			BooleanQuery boolPhrase = new BooleanQuery();
			boolPhrase.add(queryForPhrase(FIELDS_TO_SEARCH, phrase), Occur.MUST);
			boolPhrase.add(new TermRangeQuery(TextFileIndexEngine.FIELD_DATE, startDate, endDate, true, true), Occur.MUST);
			booleanQuery.add(boolPhrase, Occur.SHOULD);

		}
		return booleanQuery;
	}

	public static String saveResult(ScoreDoc[] hits, IndexSearcher indexSearcher,
			Query query) throws CorruptIndexException, IOException {
		String queryString = query.toString();
		System.out.println(queryString + "\n" + hits.length + " hit(s):");
		Matcher matcher = KEYWORD_EXTRACT_PATTERN.matcher(queryString);

		queryString = queryString.replace("+", "").replace("[", "")
				.replace("]", "").replace(":", "").replace(" ", "")
				.replace("(", "").replace(")", "");
		int queryLength = queryString.length();
		//		queryString = queryString.substring(queryLength - 14, queryLength);

		Set<String> keywords = new HashSet<String>();
		int start = 0;
		while (matcher.find(start)) {
			keywords.add(matcher.group(1));
			start = matcher.toMatchResult().end(0);
		}
		String[] keyWords = new String[keywords.size()];
		keywords.toArray(keyWords);

		for (String keyword : keyWords) {
			queryString = keyword + "_" + queryString;
		}

		String path = RESULT_DIRECTORY + FILE_SEPARATOR + queryString + ".txt";

		StringBuilder stringBuilder = new StringBuilder();
		int cout = 0;
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document doc = indexSearcher.doc(docId);
			String text = doc.get(TextFileIndexEngine.FIELD_COMMENT);
			//System.out.println(text);
			//text = StringUtils.fromFile(new File(text));
			List<String> hightlighted = getHightlighted(text, keyWords);
			for (String string : hightlighted) {
				stringBuilder.append(string).append("\r\n\r\n");
			}
		}

		StringUtils.toFile(stringBuilder.toString(), new File(path));
		return path;
	}

	public String saveResult1(ScoreDoc[] hits, IndexSearcher indexSearcher,
			Query query) throws CorruptIndexException, IOException {
		String queryString = query.toString();
		System.out.println(queryString + "\n" + hits.length + " hit(s):");
		Matcher matcher = KEYWORD_EXTRACT_PATTERN.matcher(queryString);

		queryString = queryString.replace("+", "").replace("[", "")
				.replace("]", "").replace(":", "").replace(" ", "")
				.replace("(", "").replace(")", "");
		int queryLength = queryString.length();
		//		queryString = queryString.substring(queryLength - 14, queryLength);

		Set<String> keywords = new HashSet<String>();
		int start = 0;
		while (matcher.find(start)) {
			keywords.add(matcher.group(1));
			start = matcher.toMatchResult().end(0);
		}
		String[] keyWords = new String[keywords.size()];
		keywords.toArray(keyWords);

		for (String keyword : keyWords) {
			queryString = keyword + "_" + queryString;
		}

		String path = RESULT_DIRECTORY + FILE_SEPARATOR + queryString + FILE_SEPARATOR + queryString;
		File outputFolder = new File(path);
		if (!outputFolder.exists()) {
			outputFolder.mkdirs();
		} else {
			for(File file: outputFolder.listFiles()) {
				file.delete();
			}
		}

		int cout = 0;

		int k = 0;
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document doc = indexSearcher.doc(docId);
			String text = doc.get(TextFileIndexEngine.FIELD_COMMENT);
			Date datePost = null;
			try {
				datePost = new SimpleDateFormat("yyMMdd").parse(doc.get(TextFileIndexEngine.FIELD_DATE).substring(0, 6));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			List<String> hightlighted = getHightlighted(text, keyWords);
			Set<String> temp = new HashSet<String>();
			for (String string : hightlighted) {
				temp.add(string);
				File fileOfSentence = new File(outputFolder, "" + k + ".txt");
				k++;
				Writer writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(fileOfSentence, false), "UTF-8"));
				writer.write(string);
				writer.close();
			}
			for(String s: temp){
				postTested post = new postTested();
				post.setContent(s);
				post.setDatePost(datePost);
				resultSearch.add(post);
			}
		}
		indexSearcher.getIndexReader().close();
		indexSearcher.close();
		return path;
	}

	public static List<String> getHightlighted(String contentOrigin, String[] keyWords) {
		if(!USESENTENCE){
			List<String> result = new ArrayList<String>();
			result.add(contentOrigin);
			return result;
		}
		else{
			List<String> sentences = new ArrayList<String>();

			String content = contentOrigin.concat("\n");

			Pattern pattern = Pattern.compile("[^…\\r\\n\\.!?]+[…\\r\\n\\.!?]+");
			Matcher matcher = pattern.matcher(content);
			int start = 0;
			while (matcher.find(start)) {
				String sentence = matcher.group(0).trim();
				if (sentence.length() > 0) {
					sentences.add(sentence);
				}
				start = matcher.toMatchResult().end(0);
			}

			List<String> result = new ArrayList<String>();

			Set<Integer> hits = new TreeSet<Integer>();

			for (int i = 0; i < sentences.size(); i++) {
				for (String keyWord : keyWords) {
					if (sentences.get(i).toLowerCase()
							.indexOf(keyWord.toLowerCase()) > -1) {
						if (i > 0) {
							hits.add(i - 1);
						}
						hits.add(i);
						if (i < (sentences.size() - 1)) {
							hits.add(i + 1);
						}
					}
				}
			}

			StringBuffer hightlighted = new StringBuffer();

			Iterator<Integer> hit = hits.iterator();
			Integer prev = null;
			while (hit.hasNext()) {
				int current = hit.next();
				if (prev != null && current - prev > 1) {
					result.add(hightlighted.toString());
					hightlighted.setLength(0);
				}
				prev = current;
				hightlighted.append(sentences.get(current)).append(' ');
			}

			if (hightlighted.length() > 0)
				result.add(hightlighted.toString());
			return result;
		}
	}
}