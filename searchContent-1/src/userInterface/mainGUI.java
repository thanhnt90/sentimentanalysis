package userInterface;

import gate.util.GateException;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import org.jfree.ui.RefineryUtilities;

import Entities.postTested;

import net.sourceforge.jeval.EvaluationException;

import crawler.StringUtils;

import useDictionary.Tagger;
import weka.ClassifierWrapper;
import weka.TextCategorizationTest;
import weka.WekaClassifier;
import weka.classifiers.Classifier;

import lusenceSearch.TextFileSearch;

public class mainGUI extends JFrame  implements ItemListener, ActionListener{
	//date component
	private ClassifierWrapper classifierWrapper;
	private JComboBox startYear, startMonth, startDay;
	private JComboBox endYear, endMonth, endDay;
	private Calendar startDate = Calendar.getInstance();
	private Calendar endDate = Calendar.getInstance();

	//keyword component
	private JTextField keyWordText;
	private JTextArea keywordList;

	//solution component
	private JComboBox algorithmLearning;
	private JComboBox otherSolution;
	JCheckBox selectSVM = new JCheckBox();
	JCheckBox selectOther = new JCheckBox();

	//result component
	private JTextArea resultSearchTxt;
	private JTable resultTable;
	private JTextArea resultOpinion2Txt;

	private List<String> keyWords;
	private TextFileSearch searcher;

	/**
	 * This method sets up the New File Dialog window
	 */
	public mainGUI() {

		keyWords = new ArrayList<String>();
		searcher = new TextFileSearch();

		//date panel
		JPanel datesPanel = new JPanel();
		datesPanel.setBorder(BorderFactory.createTitledBorder("Dates"));

		JPanel datesInnerPanel = new JPanel(new GridLayout(2, 4, 10, 10));
		JLabel startDateLabel = new JLabel("Start Date:  ", SwingConstants.RIGHT);
		startYear = new JComboBox();
		buildYearsList(startYear);
		startYear.setSelectedIndex(5);
		startMonth = new JComboBox();
		buildMonthsList(startMonth);
		startMonth.setSelectedIndex(startDate.get(Calendar.MONTH));
		startDay = new JComboBox();
		buildDaysList(startDate, startDay, startMonth);
		startDay.setSelectedItem(Integer.toString(startDate.get(Calendar.DATE)));
		startYear.addItemListener(this);
		startMonth.addItemListener(this);
		startDay.addItemListener(this);
		datesInnerPanel.add(startDateLabel);
		datesInnerPanel.add(startDay);
		datesInnerPanel.add(startMonth);
		datesInnerPanel.add(startYear);
		JLabel endDateLabel = new JLabel("End Date:  ", SwingConstants.RIGHT);
		endYear = new JComboBox();
		buildYearsList(endYear);
		endYear.setSelectedIndex(5);
		endMonth = new JComboBox();
		buildMonthsList(endMonth);
		endMonth.setSelectedIndex(endDate.get(Calendar.MONTH));
		endDay = new JComboBox();
		buildDaysList(endDate, endDay, endMonth);
		endDay.setSelectedItem(Integer.toString(endDate.get(Calendar.DATE)));
		endYear.addItemListener(this);
		endMonth.addItemListener(this);
		endDay.addItemListener(this);
		datesInnerPanel.add(endDateLabel);
		datesInnerPanel.add(endDay);
		datesInnerPanel.add(endMonth);
		datesInnerPanel.add(endYear);
		datesPanel.add(datesInnerPanel, BorderLayout.CENTER);

		//word panel
		JPanel wordPanel = new JPanel();
		wordPanel.setBorder(BorderFactory.createTitledBorder("Input"));

		JLabel keyWorldLabel = new JLabel("Keyword:  ", SwingConstants.RIGHT);
		keyWordText = new JTextField("", 15);
		JButton addButton = new JButton("Add");
		addButton.setActionCommand("addWord");
		JLabel allwordLbl = new JLabel("All keyword:  ", SwingConstants.RIGHT);
		keywordList = new JTextArea();
		keywordList.setEditable(false);
		keywordList.setLineWrap(true);
		keywordList.setWrapStyleWord(true);  
		JScrollPane scroll = new JScrollPane(keywordList);
		JButton resetButton = new JButton("Reset");
		resetButton.setActionCommand("reset");
		addButton.addActionListener(this);
		resetButton.addActionListener(this);

		JPanel wordInnerPanel = new JPanel();
		wordInnerPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		wordInnerPanel.add(keyWorldLabel, c);
		c.gridx = 1;
		c.gridy = 0;
		wordInnerPanel.add(keyWordText, c);
		c.gridx = 2;
		c.gridy = 0;
		wordInnerPanel.add(addButton, c);
		c.gridx = 0;
		c.gridy = 1;
		c.gridheight = 2;
		wordInnerPanel.add(allwordLbl, c);
		c.gridx = 1;
		c.gridy = 1;
		c.ipady = 40;
		wordInnerPanel.add(scroll, c);
		c.gridx = 2;
		c.gridy = 1;
		c.ipady = 0;
		wordInnerPanel.add(resetButton, c);
		wordPanel.add(wordInnerPanel, BorderLayout.CENTER);

		//algorithm panel
		JPanel algorithmPanel = new JPanel();
		algorithmPanel.setBorder(BorderFactory.createTitledBorder("Algorithm"));
		algorithmLearning = new JComboBox();
		String algName = "weka.classifiers.trees.ADTree," +
				"weka.classifiers.bayes.AODE," +
				"weka.classifiers.bayes.BayesNet," +
				"weka.classifiers.bayes.ComplementNaiveBayes," +
				"weka.classifiers.rules.ConjunctiveRule," +
				"weka.classifiers.trees.DecisionStump," +
				"weka.classifiers.rules.DecisionTable," +
				"weka.classifiers.misc.HyperPipes," +
				"weka.classifiers.lazy.IB1," +
				"weka.classifiers.lazy.IBk," +
				"weka.classifiers.trees.Id3," +
				"weka.classifiers.trees.J48," +
				"weka.classifiers.rules.JRip," +
				"weka.classifiers.lazy.KStar," +
				"weka.classifiers.lazy.LBR," +
				"weka.classifiers.functions.LeastMedSq," +
				"weka.classifiers.functions.LinearRegression," +
				"weka.classifiers.trees.LMT," +
				"weka.classifiers.functions.Logistic," +
				"weka.classifiers.trees.lmt.LogisticBase," +
				"weka.classifiers.trees.m5.M5Base," +
				"weka.classifiers.functions.MultilayerPerceptron," +
				"weka.classifiers.MultipleClassifiersCombiner," +
				"weka.classifiers.bayes.NaiveBayes," +
				"weka.classifiers.bayes.NaiveBayesMultinomial," +
				"weka.classifiers.bayes.NaiveBayesSimple," +
				"weka.classifiers.trees.NBTree," +
				"weka.classifiers.rules.NNge," +
				"weka.classifiers.rules.OneR," +
				"weka.classifiers.functions.PaceRegression," +
				"weka.classifiers.rules.PART," +
				"weka.classifiers.trees.m5.PreConstructedLinearModel" +
				"weka.classifiers.rules.Prism," +
				"weka.classifiers.trees.RandomForest," +
				"weka.classifiers.RandomizableClassifier," +
				"weka.classifiers.trees.RandomTree," +
				"weka.classifiers.functions.RBFNetwork," +
				"weka.classifiers.trees.REPTree," +
				"weka.classifiers.rules.Ridor," +
				"weka.classifiers.trees.m5.RuleNode.html" +
				"weka.classifiers.functions.SimpleLinearRegression," +
				"weka.classifiers.functions.SimpleLogistic," +
				"weka.classifiers.SingleClassifierEnhancer," +
				"weka.classifiers.functions.SMO," +
				"weka.classifiers.functions.SMOreg," +
				"weka.classifiers.trees.UserClassifier," +
				"weka.classifiers.misc.VFI," +
				"weka.classifiers.functions.VotedPerceptron," +
				"weka.classifiers.functions.Winnow," +
				"weka.classifiers.rules.ZeroR";
		for(String AlgName: algName.split(","))
			algorithmLearning.addItem(AlgName);
		algorithmLearning.setSelectedIndex(41);
		otherSolution = new JComboBox();
		otherSolution.addItem("Use dictionary");
		otherSolution.setEnabled(false);
		selectOther.setSelected(false);
		selectSVM.setSelected(true);

		JPanel otherSolutionPanel = new JPanel();
		selectOther.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox temp = (JCheckBox)e.getSource();
				selectSVM.setSelected(!temp.isSelected());
				otherSolution.setEnabled(temp.isSelected());
				algorithmLearning.setEnabled(!temp.isSelected());

			}
		});
		otherSolutionPanel.add(new JLabel("Other"));
		otherSolutionPanel.add(selectOther);
		otherSolutionPanel.add(otherSolution);

		JPanel svmPanel = new JPanel();
		selectSVM.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JCheckBox temp = (JCheckBox)e.getSource();
				selectOther.setSelected(!temp.isSelected());
				otherSolution.setEnabled(!temp.isSelected());
				algorithmLearning.setEnabled(temp.isSelected());
			}
		});
		svmPanel.add(new JLabel("SVM"));
		svmPanel.add(selectSVM);
		svmPanel.add(algorithmLearning);
		algorithmPanel.setLayout(new GridLayout(1, 4, 10, 10));
		algorithmPanel.add(otherSolutionPanel);
//		algorithmPanel.add(new JLabel());
//		algorithmPanel.add(new JLabel());
		algorithmPanel.add(svmPanel);

		//panel for all input
		JPanel topPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c2 = new GridBagConstraints();
		c2.weightx = 0.5;
		c2.fill = GridBagConstraints.HORIZONTAL;
		c2.gridx = 0;
		c2.gridy = 0;
		topPanel.add(datesPanel, c2);
		c2.gridx = 1;
		c2.gridy = 0;
		topPanel.add(wordPanel, c2);
		c2.gridx = 0;
		c2.gridy = 1;
		c2.gridwidth = 2;	
		topPanel.add(algorithmPanel, c2);

		//result panel
		JPanel resultPanel = new JPanel(new BorderLayout());
		resultPanel.setBorder(BorderFactory.createTitledBorder("Result"));
		JButton searchButton = new JButton("Search");
		searchButton.setActionCommand("search");
		searchButton.addActionListener(this);
		JPanel btnPanle = new JPanel();
		resultSearchTxt = new JTextArea(); 
		Dimension dim3 = new Dimension(50, 30);
		btnPanle.add(searchButton);
		resultTable = new JTable();
		resultTable.setDefaultRenderer(String.class, new MultiLineTableCellRenderer());
		resultTable.setFont(new Font("Tomaho", Font.PLAIN, 18));
		resultTable.setAutoCreateRowSorter(true);
		TableRowSorter<TableModel> sorter  = new TableRowSorter(resultTable.getModel());
		resultTable.setRowSorter(sorter);

		resultOpinion2Txt = new JTextArea();
		JScrollPane mainResul = new JScrollPane(resultTable);
		JButton saveButton = new JButton("Save");
		saveButton.setActionCommand("save");
		saveButton.addActionListener(this);
		JButton graphButton = new JButton("Graph");
		graphButton.setActionCommand("graph");
		graphButton.addActionListener(this);
		JPanel btnSavePanle = new JPanel();
		btnSavePanle.add(saveButton);
		btnSavePanle.add(graphButton);

		JPanel displayPanel = new JPanel();
		displayPanel.setBackground(new Color(255, 0, 0, 255));
		displayPanel.setLayout(new GridBagLayout());
		GridBagConstraints c1 = new GridBagConstraints();
		c1.weightx = 0.5;
		c1.fill = GridBagConstraints.HORIZONTAL;
		c1.gridx = 0;
		c1.gridy = 0;
		displayPanel.add(resultSearchTxt, c1);
		c1.gridx = 0;
		c1.gridy = 1;
		c1.ipady = 150;
		displayPanel.add(mainResul, c1);
		c1.gridx = 0;
		c1.gridy = 2;
		c1.ipady = 0;
		c1.ipadx = 0;
		c1.gridheight = 1;
		displayPanel.add(resultOpinion2Txt, c1);
		c1.gridx = 0;
		c1.gridy = 3;
		c1.ipady = 0;
		c1.gridheight = 1;
		displayPanel.add(btnSavePanle, c1);


		resultPanel.add("East", new Box.Filler(dim3, dim3, dim3));
		resultPanel.add("West", new Box.Filler(dim3, dim3, dim3));
		resultPanel.add("North", btnPanle);
		resultPanel.add("Center", displayPanel);
		resultPanel.add("South", new Box.Filler(dim3, dim3, dim3));

		JPanel bottomPanel = new JPanel(new BorderLayout());
		bottomPanel.add("Center", resultPanel);

		JPanel centerPanel = new JPanel(new BorderLayout());
		centerPanel.add("Center", topPanel);
		centerPanel.add("South", bottomPanel);

		getContentPane().setLayout(new BorderLayout());
		Dimension dim = new Dimension(10, 10);
		getContentPane().add("North", new Box.Filler(dim, dim, dim));
		getContentPane().add("West", new Box.Filler(dim, dim, dim));
		getContentPane().add("Center", centerPanel);
		getContentPane().add("East", new Box.Filler(dim, dim, dim));
		getContentPane().add("South", new Box.Filler(dim, dim, dim));

		pack();

		setSize(new Dimension(900, 600));
		double parentWidth = this.getSize().getWidth();
		double parentHeight = this.getSize().getHeight();
		double dialogWidth = this.getSize().getWidth();
		double dialogHeight = this.getSize().getHeight();

		setLocation((int)(parentWidth / 2 - dialogWidth / 2),
				(int)(parentHeight / 2 - dialogHeight / 2));

		setResizable(true);
		setVisible(true);
	}

	/**
	 * This method builds the list of years for the start
	 * date and end date of the semester
	 * @param yearsList The combo box containing the years
	 */
	private void buildYearsList(JComboBox yearsList) {

		int currentYear = startDate.get(Calendar.YEAR);

		for (int yearCount = currentYear - 5; yearCount <= currentYear + 5; yearCount++)
			yearsList.addItem(Integer.toString(yearCount));
	}

	/**
	 * This method builds the list of months for the start
	 * date and end date of the semester
	 * @param monthsList The combo box containing the months
	 */
	private void buildMonthsList(JComboBox monthsList) {

		monthsList.removeAllItems();
		for (int monthCount = 0; monthCount < 12; monthCount++){
			DecimalFormat df1 = new DecimalFormat("00");
			monthsList.addItem(df1.format(monthCount + 1));
		}
		//            monthsList.addItem(monthCount + 1);
	}

	/**
	 * This method builds the list of years for the start
	 * date and end date of the semester
	 * @param dateIn The current date, which will be used for
	 * the initial date of the lists
	 * @param daysList The combo box that will contain the days
	 * @param monthsList The combo box that will contain the months
	 */
	private void buildDaysList(Calendar dateIn, JComboBox daysList, JComboBox monthsList) {

		daysList.removeAllItems();
		dateIn.set(Calendar.MONTH, monthsList.getSelectedIndex());
		int lastDay = startDate.getActualMaximum(Calendar.DAY_OF_MONTH);

		for (int dayCount = 1; dayCount <= lastDay; dayCount++){
			DecimalFormat df1 = new DecimalFormat("00");
			daysList.addItem(df1.format(dayCount));
		}
	}

	/**
	 * This method is called when a dropdown selection
	 * changes
	 * @param event This occurs when a dropdown changes values
	 */
	public void itemStateChanged(ItemEvent event) {

		if (event.getSource() == startYear &&
				event.getStateChange() == ItemEvent.SELECTED) {

			int year = Integer.parseInt((String)startYear.getSelectedItem());
			startDate.set(Calendar.YEAR, year);
			startMonth.setSelectedIndex(0);
			startDate.set(Calendar.MONTH, 0);
			buildDaysList(startDate, startDay, startMonth);
			startDate.set(Calendar.DATE, 1);
		}
		else if (event.getSource() == startMonth &&
				event.getStateChange() == ItemEvent.SELECTED) {

			startDate.set(Calendar.MONTH, startMonth.getSelectedIndex());
			buildDaysList(startDate, startDay, startMonth);
			startDate.set(Calendar.DATE, 1);
		}
		else if (event.getSource() == startDay &&
				event.getStateChange() == ItemEvent.SELECTED) {

			int day = Integer.parseInt((String)startDay.getSelectedItem());
			startDate.set(Calendar.DATE, day);
		}
		else if (event.getSource() == endYear &&
				event.getStateChange() == ItemEvent.SELECTED) {

			int year = Integer.parseInt((String)endYear.getSelectedItem());
			endDate.set(Calendar.YEAR, year);
			endMonth.setSelectedIndex(0);
			endDate.set(Calendar.MONTH, 0);
			buildDaysList(endDate, endDay, endMonth);
			endDate.set(Calendar.DATE, 1);
		}
		else if (event.getSource() == endMonth &&
				event.getStateChange() == ItemEvent.SELECTED) {

			endDate.set(Calendar.MONTH, endMonth.getSelectedIndex());
			buildDaysList(endDate, endDay, endMonth);
			endDate.set(Calendar.DATE, 1);
		}
		else if (event.getSource() == endDay &&
				event.getStateChange() == ItemEvent.SELECTED) {

			int day = Integer.parseInt((String)endDay.getSelectedItem());
			endDate.set(Calendar.DATE, day);
		}
	}

	public static void main(String[] args) {
		mainGUI app = new mainGUI();
		app.setVisible(true);
		app.pack();
	}

	public void displayAllKeyWord(){
		StringBuffer str = new StringBuffer();
		for(String s: keyWords){
			if(str.length() > 0 && s.length() > 0){
				str.append(", ");
				str.append(s);
			}
			if(str.length() == 0)
				str.append(s);
		}
		keywordList.setText(str.toString());
	}

	public boolean isExit(String str){
		for(String s: keyWords){
			if(str.equalsIgnoreCase(s)){
				return true;
			}
		}
		return false;
	}

	public String getStartDate(){
		StringBuffer result = new StringBuffer();
		result.append(startYear.getSelectedItem().toString().substring(2));
		result.append(startMonth.getSelectedItem().toString());
		result.append(startDay.getSelectedItem().toString());
		return result.toString();
	}

	public String getEndDaate(){
		StringBuffer result = new StringBuffer();
		result.append(endYear.getSelectedItem().toString().substring(2));
		result.append(endMonth.getSelectedItem().toString());
		result.append(endDay.getSelectedItem().toString());
		return result.toString();
	}

	@Override
	public void actionPerformed(ActionEvent actEvent) {
		if(((JButton)actEvent.getSource()).getActionCommand().equalsIgnoreCase("addWord")){
			if(keyWordText.getText().length() > 0 && !isExit(keyWordText.getText())){
				keyWords.add(keyWordText.getText());
				displayAllKeyWord();
				keyWordText.setText("");
			}
		}
		if(((JButton)actEvent.getSource()).getActionCommand().equalsIgnoreCase("reset")){
			System.out.println("aafadfa");
			keyWordText.setText("");
			keyWords.clear();
			keywordList.setText("");
		}
		if(((JButton)actEvent.getSource()).getActionCommand().equalsIgnoreCase("search")){
			System.out.println(getStartDate() + "   " + getEndDaate());
			resultSearchTxt.setText("Searching...");
			if(getStartDate().compareToIgnoreCase(getEndDaate()) > 0){
				JOptionPane.showMessageDialog(null, "StartDate is after EndDate!", "Error", JOptionPane.ERROR_MESSAGE);
				resultSearchTxt.setText("");

			}
			else{
				long numberResult = 0;
				if(keyWords.size() > 0)
					numberResult = searcher.search(keyWords, getStartDate(), getEndDaate());
				resultSearchTxt.setText("Number of result: " + numberResult);
//				TextCategorizationTest classify = new TextCategorizationTest();
				
				try {
					classifierWrapper = new WekaClassifier(
							(String)algorithmLearning.getSelectedItem(),
							new File("//Users//ThanhNT//Desktop//trainData//trainAll"));

				} catch (Exception e) {
					e.printStackTrace();
				}
				ArrayList<postTested> listPost = null;
				
				ArrayList<String> listSentence = new ArrayList<String>();
				ArrayList<String> resultClassified = null;
				String[] classes = null;
				
				listPost = searcher.getResultSearch();
				for(postTested post:listPost){
					listSentence.add(post.getContent());
				}
				System.out.println("search done");
				if(selectSVM.isSelected()){
					long startTime = System.currentTimeMillis();
					resultClassified = classifierWrapper.classify(listSentence);
					
					System.out.println("classify done!" + (System.currentTimeMillis() - startTime));
					Set<String> classSet = classifierWrapper.getClasses();
					classes = new String[classSet.size()];
					classSet.toArray(classes);
				}
				else{
					long startTime = System.currentTimeMillis();
					try {
						resultClassified = Tagger.classifierUsingDictionary(listSentence, keywordList.getText());
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("classify done!" + (System.currentTimeMillis() - startTime));
					classes = new String[3];
					classes[0] = "neg";
					classes[1] = "pos";
					classes[2] = "unknow";
				}
				
				for(int i = 0; i < listPost.size(); i++){
					listPost.get(i).setClazz(resultClassified.get(i));
				}
				
				resultTable.setModel(new TableModel(listPost, classes));
				
				DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
				centerRenderer.setHorizontalAlignment( JLabel.CENTER );
				resultTable.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
				System.out.println("render table done");
				TableColumn column = null;
				for (int i = 0; i < resultTable.getModel().getColumnCount(); i++) {
					column = resultTable.getColumnModel().getColumn(i);
					if(i == 0)
						column.setPreferredWidth(40);
					if (i == 1) {
						column.setPreferredWidth(1000); //second column is bigger
					} else {
						column.setPreferredWidth(70);
					}
				}
				resultOpinion2Txt.setText("Analysis results: " + ((TableModel)resultTable.getModel()).resultOpinion());
			}

		}
		if(((JButton)actEvent.getSource()).getActionCommand().equalsIgnoreCase("save")){
			if(resultTable.getModel().getClass().equals(TableModel.class)){
				((TableModel)resultTable.getModel()).addToTrainData();
			}
		}
		if(((JButton)actEvent.getSource()).getActionCommand().equalsIgnoreCase("graph")){
			PeriodAxisDemo3 demo = new PeriodAxisDemo3("Period Axis Demo 3", ((TableModel)resultTable.getModel()).getListPost());
			demo.pack();
			RefineryUtilities.centerFrameOnScreen(demo);
			demo.setVisible(true);
		}
	}
	
	public static Set<String> sentenceInFolder(File folder){
		Set<String> result = new HashSet<String>();
		if(!folder.isDirectory()){
			if(!folder.getName().startsWith("."))
			result.add(StringUtils.fromFile(folder));
		}
		else{
			for(File f:folder.listFiles())
				result.addAll(sentenceInFolder(f));
		}
		return result;
	}
	
	
}

