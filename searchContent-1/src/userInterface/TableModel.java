package userInterface;

import indexAndSearch.TextIndexEngine;
import indexAndSearch.searchFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.table.AbstractTableModel;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.store.LockObtainFailedException;

import Entities.postTested;

import crawler.StringUtils;

public class TableModel extends AbstractTableModel {
	protected TableModel base;

	protected int sortColumn;

	protected int[] row;

	private ArrayList<postTested> listPost;

	public static String FOLDER_TRAIN = "//Users//ThanhNT//Desktop//trainData//trainTechData";
	public static String FOLDER_TRAIN_INDEX = "//Users//ThanhNT//Desktop//indexTrainData"; 

	public static String FOLDER_TRAIN_OBJ = "//Users//ThanhNT//Desktop//trainData//trainSubAndObj//Objective"; 
	public static String FOLDER_TRAIN_POS = "//Users//ThanhNT//Desktop//trainData//trainPosAndNeg//pos"; 
	public static String FOLDER_TRAIN_NEG = "//Users//ThanhNT//Desktop//trainData//trainPosAndNeg//neg"; 

	public static final String FILE_SEPARATOR = System.getProperty("file.separator");

	public ArrayList<String> CLAZZ = new ArrayList<String>();

	public ArrayList<int[]> stateEdit = new ArrayList<int[]>();
	public ArrayList<boolean[]> stateBegin = new ArrayList<boolean[]>();

	//	public static String[] COLUMN_NAMES = {"text", "positive", "negative", "in positiveTrain", "in negativeTrain"};

	public ArrayList<String> columnName(){

		ArrayList<String> result = new ArrayList<String>();
		result.add("SN");
		result.add("Text");
		for(String s: CLAZZ){
			result.add(s);
		}
		for(String s: CLAZZ){
			result.add("in " + s);
		}
		return result;
	}
	public TableModel(){};
	public TableModel(ArrayList<postTested> listPost, String[] columName) {
		super();
		int incorrect = 0;
		int correct = 0;
		this.listPost = listPost;
		for(String s:columName){
			CLAZZ.add(s);
		}
		for(String str:CLAZZ)
			System.out.println(str);
		//state begin
		
		for(int i = 0; i < listPost.size(); i++){
			boolean [] isInFolder = new boolean[CLAZZ.size()];
			for(int j = 0; j < CLAZZ.size(); j++){
//				isInFolder[j] = false ;
				isInFolder[j] = isInFolder(listPost.get(i).getContent(), new File(FOLDER_TRAIN + FILE_SEPARATOR + CLAZZ.get(j)));
				if(isInFolder[j]){
					if(!CLAZZ.get(j).equalsIgnoreCase(listPost.get(i).getClazz())){
						incorrect ++;
						//						System.out.println("incorrect location: " + i);
					}
					else{
						correct ++;
						//						System.out.println("correct location: " + i);
					}
				}
			}
			stateBegin.add(isInFolder);	
		}

		//state edit
		for(int i = 0; i < listPost.size(); i++){
			int [] isInFolder = new int[CLAZZ.size()];
			for(int j = 0; j < CLAZZ.size(); j++){
				isInFolder[j] = 0; // 0 is not edit, > 0 is copy to folder and < 0 is remove
			}
			stateEdit.add(isInFolder);	
		}
		System.out.println("All incorrect: " + incorrect + " / All correct: " + correct);

	}

	@Override
	public String getColumnName(int column) {
		return columnName().get(column);
	}

	

	public ArrayList<postTested> getListPost() {
		return listPost;
	}
	public void setListPost(ArrayList<postTested> listPost) {
		this.listPost = listPost;
	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnName().size();
	}

	@Override
	public int getRowCount() {
		return listPost.size();
	}

	@Override
	public void setValueAt(Object arg0, int row, int column) {
		if(column == 1){
			listPost.get(row).setContent((String)arg0);
		}
		if(column > CLAZZ.size() + 1){
			if((Boolean)getValueAt(row, column))
				stateEdit.get(row)[column - CLAZZ.size() - 2]--;
			else
				stateEdit.get(row)[column - CLAZZ.size() - 2]++;
		}
	};

	@Override
	public Object getValueAt(int row, int column) {
		if(column == 0)
			return row;
		if(column == 1)
			return listPost.get(row).getContent();
		if(column > 1 && column < CLAZZ.size() + 2){
			return listPost.get(row).getClazz().equals(CLAZZ.get(column - 2));
		}

		return currentState(stateBegin.get(row)[column - CLAZZ.size() - 2], stateEdit.get(row)[column - CLAZZ.size() - 2]);
	}

	public boolean currentState(boolean beginState, int edit){
		if(edit > 0){
			if(beginState) return beginState;
			else return !beginState;
		}
		if(edit < 0){
			if(beginState) return !beginState;
			else return beginState;
		}
		return beginState;
	}

	@Override
	public Class getColumnClass(int column) {
		if(column == 0)
			return Integer.class;
		if (column > 1)
			return Boolean.class;
		return String.class;
	}
	@Override
	public boolean isCellEditable(int row, int column) {
		if (column < 2 || column > CLAZZ.size() + 1)
			return true;
		return false;
	}

	int dsds = 0;
	public boolean isInFolder(String str, File folder){
//		System.out.println("checked!" + (dsds++));
//		return searchFile.searchStringInFolder(str, folder, FOLDER_TRAIN_INDEX);
		for(File f: folder.listFiles()){
			String contentOfFile = StringUtils.fromFile(f);
			if(contentOfFile.indexOf(str) > -1 || str.indexOf(contentOfFile) > -1){
				//				System.out.println(folder.getName());
				return true;
			}
		}
		return false;
	}

	public int getMinDistance(String str, File folder){
		int result = Integer.MAX_VALUE;
		for(File f: folder.listFiles()){
			int temp = org.apache.commons.lang3.StringUtils.getLevenshteinDistance(str, StringUtils.fromFile(f));
			if(temp < result)
				result = temp;
		}
		return result;
	}

	public void addToTrainData(){
		for(int i = 0; i < stateEdit.size(); i++){
			for(int j = 0; j < stateEdit.get(i).length; j++){
				if(stateEdit.get(i)[j] > 0){
					String fileName = String.valueOf(nameFileToSave(new File(FOLDER_TRAIN + FILE_SEPARATOR + CLAZZ.get(j)))) + ".txt";
					File saveFile = new File(FOLDER_TRAIN + FILE_SEPARATOR + CLAZZ.get(j) + FILE_SEPARATOR + fileName); 
					StringUtils.toFile((String)getValueAt(i, 1), saveFile);
					stateEdit.get(i)[j] = 0;
					stateBegin.get(i)[j] = true;
					try {
						TextIndexEngine.indexData(saveFile, new File(FOLDER_TRAIN_INDEX));
					} catch (CorruptIndexException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (LockObtainFailedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	public int nameFileToSave(File folder){
		int result = -1;
		for(File file: folder.listFiles()){
			if(file.getName().charAt(0) != '.'){
				int temp = Integer.parseInt(file.getName().substring(0, file.getName().lastIndexOf(".txt")));
				if(temp > result)
					result = temp;
			}
		}
		return result + 1;
	}

	public String resultOpinion(){
		int numberOfSentence = listPost.size();
		int numberSentencePerClazz[] = new int[CLAZZ.size()];
		for(postTested post:listPost){
			for(int i = 0; i < CLAZZ.size(); i++)
				if(post.getClazz().equalsIgnoreCase(CLAZZ.get(i))){
					numberSentencePerClazz[i]++;
					break;
				}
		}
		StringBuilder strbd = new StringBuilder();
		for(int j = 0; j < CLAZZ.size(); j++){
			strbd.append(CLAZZ.get(j) + ": " + numberSentencePerClazz[j] + "/" + numberOfSentence + " (" + String.format("%.2f", (float)numberSentencePerClazz[j] * 100/numberOfSentence) + "%) ");
		}
		return strbd.toString();
	}

	public static void main(String args[]){
		TableModel temp = new TableModel();
		System.out.println(temp.isInFolder("ngọc trinh quá đẹp", new File("//Users//ThanhNT//Desktop//trainData//pos")));

	}
}
