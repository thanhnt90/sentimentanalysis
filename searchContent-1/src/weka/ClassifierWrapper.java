package weka;



import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public interface ClassifierWrapper {
	Set<String> getClasses();
	public ArrayList<String> classify(List<String> txts);
}
