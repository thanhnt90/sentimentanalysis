package weka;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import weka.SMARTStringToWordsFilter.WeightingScheme;
import weka.classifiers.Classifier;
import weka.classifiers.functions.SMO;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.TextDirectoryLoader;
import weka.core.tokenizers.NGramTokenizer;
import weka.core.tokenizers.Tokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class TextCategorizationTest {
	private ArrayList<String> listSentences;
	private ArrayList<String> clazzOfSentence;
	private float accurate;
	

	public float getAccurate() {
		return accurate;
	}

	public void setAccurate(float accurate) {
		this.accurate = accurate;
	}

	public ArrayList<String> getListSentences() {
		return listSentences;
	}

	public void setListSentences(ArrayList<String> listSentences) {
		this.listSentences = listSentences;
	}

	public ArrayList<String> getClazzOfSentence() {
		return clazzOfSentence;
	}

	public void setClazzOfSentence(ArrayList<String> clazzOfSentence) {
		this.clazzOfSentence = clazzOfSentence;
	}

	/**
	 * Expects the first parameter to point to the directory with the text
	 * files. In that directory, each sub-directory represents a class and the
	 * text files in these sub-directories will be labeled as such.
	 * 
	 * @param args
	 *            the commandline arguments
	 */
	public static void main(String[] args) {
		
		try {
			// convert the directory into a dataset
			TextDirectoryLoader loader = new TextDirectoryLoader();
			loader.setDirectory(new File("//Users//ThanhNT//Desktop//trainData"));
			loader.setCharSet("UTF-8");
			Instances train = loader.getDataSet();

			Filter filter = new StringToWordVector();
			Tokenizer tokenizer = new NGramTokenizer();
			
			((StringToWordVector) filter).setTokenizer(tokenizer);
			filter.setInputFormat(train);
			train.setClassIndex(0);
			System.out.println(train.instance(1).stringValue(0));
			train = Filter.useFilter(train, filter);
			train.setClassIndex(0);
			saveArff(train, new File("//Users//ThanhNT//Desktop//training.arff"));
			
			
//			System.out.println(train.instance(1));

			Classifier classifier = new SMO();
			
			classifier.buildClassifier(train);

			loader.setDirectory(new File("//Users//ThanhNT//Desktop//demo_result//untitled folder"));
			loader.setCharSet("UTF-8");
			Instances test = loader.getDataSet();
			System.out.println(test.instance(1).stringValue(0));
			test.setClassIndex(0);
			test = Filter.useFilter(test, filter);
			
			System.out.println(test.instance(1).stringValue(0));
			


			saveArff(test, new File("D:\\Workspace\\Desktop\\test.arff"));

			float count = 0;
			int i = 0;
			for (; i < test.size(); ++i) {
				String classifiedAs = classifier.classifyInstance(test.get(i)) == 0 ? "neg"
						: "pos";
				String clazz = test.get(i).stringValue(0);

				System.out.print(classifiedAs);

				if (clazz.equals(classifiedAs)) {
					count++;
					System.out.println(" (true)");
				} else {
					System.out.println(" vs " + clazz);
				}
			}
//			System.err.println(classifier.getClass().getSimpleName() + ": "
//					+ ((100 * count) / i) + "%");

			/*
			 * FilteredClassifier classifier = new FilteredClassifier();
			 * classifier.setFilter(filter); Classifier classifiers[] = {new
			 * SMO(), new BayesNet(), new J48()}; for (Classifier
			 * innerClassifier: classifiers){
			 * classifier.setClassifier(innerClassifier);
			 * classifier.buildClassifier(train); loader.setDirectory(new
			 * File("D:\\Workspace\\Desktop\\test"));
			 * loader.setCharSet("UTF-8"); Instances test = loader.getDataSet();
			 * 
			 * float count = 0; int i = 0; for (; i<test.size(); ++i) { String
			 * classifiedAs = classifier.classifyInstance(test.get(i)) == 0 ?
			 * "neg" : "pos"; String clazz = test.get(i).stringValue(1);
			 * System.out.print(test.get(i).stringValue(0) + '\n' +
			 * classifiedAs); if(clazz.equals(classifiedAs)) { count++;
			 * System.out.println(" (true)"); } else { System.out.println(" vs "
			 * + clazz); } }
			 * System.err.println(innerClassifier.getClass().getSimpleName() +
			 * ": " + ((100 * count)/i) + "%"); }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static ArrayList<String> classifiFolder(String folderPath, String algname){
		ArrayList<String> result = new ArrayList<String>();
		try {
			// convert the directory into a dataset
			TextDirectoryLoader loader = new TextDirectoryLoader();
			loader.setDirectory(new File("//Users//ThanhNT//Desktop//trainData//trainAll"));
			loader.setCharSet("UTF-8");
			Instances train = loader.getDataSet();

			Filter filter = new StringToWordVector();
			Tokenizer tokenizer = new NGramTokenizer();
			((StringToWordVector) filter).setTokenizer(tokenizer);
			filter.setInputFormat(train);
			train = Filter.useFilter(train, filter);
			
			Class theClass  = Class.forName(algname);
			Classifier classifier = (Classifier)theClass.newInstance();
			classifier.buildClassifier(train);

			loader.setDirectory(new File(folderPath));
			loader.setCharSet("UTF-8");
			Instances test = loader.getDataSet();
//			test.setClassIndex(1);
			System.out.println("-----------" + test.instance(2).classIndex());
			test = Filter.useFilter(test, filter);
			
			System.out.println("+++++++++++++++" + test.instance(2).classIndex());

			float count = 0;
			int i = 0;
			for (; i < test.size(); ++i) {
				int clazz = (int) classifier.classifyInstance(test.get(i));
				String className = test.attribute(0).value(clazz);
				result.add(className);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	public ArrayList<String> classifiFolder3(String folderPath, String algname){
		listSentences = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		try {
			// convert the directory into a dataset
			TextDirectoryLoader loader = new TextDirectoryLoader();
			loader.setDirectory(new File("//Users//ThanhNT//Desktop//trainData//trainPosAndNeg"));
			loader.setCharSet("UTF-8");
			Instances trainSubAndObj = loader.getDataSet();
			Filter filter = new SMARTStringToWordsFilter();
			Tokenizer tokenizer = new NGramTokenizer();
			((SMARTStringToWordsFilter) filter).setTokenizer(tokenizer);
			((SMARTStringToWordsFilter) filter).setOutputWordCounts(true);
			((SMARTStringToWordsFilter) filter).setTransform(WeightingScheme.ADTSN);
			filter.setInputFormat(trainSubAndObj);
			trainSubAndObj = Filter.useFilter(trainSubAndObj, filter);
			
			Class theClass  = Class.forName(algname);
			Classifier classifier = (Classifier)theClass.newInstance();
			classifier.buildClassifier(trainSubAndObj);

			loader.setDirectory(new File(folderPath));
			loader.setCharSet("UTF-8");
			Instances beginTest = loader.getDataSet();
			Instances test = new Instances(beginTest);
//			System.out.println("-----------" + test.instance(2).classIndex());
			Instances testedSubAndObj = Filter.useFilter(test, filter);
			
			System.out.println("+++++++++++++++" + test.instance(2).classIndex());

			int i = 0;			
			for (; i < testedSubAndObj.size(); ++i) {
				int clazz = (int) classifier.classifyInstance(testedSubAndObj.get(i));
				String className = testedSubAndObj.attribute(testedSubAndObj.classIndex()).value(clazz);
				result.add(className);
				if(i < 100)
					System.out.println(className + "\t" + beginTest.instance(i).stringValue(0));				
				listSentences.add(beginTest.instance(i).stringValue(0));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
		
	public ArrayList<String> classifiFolder2(String folderPath, String algname){
		listSentences = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
		try {
			// convert the directory into a dataset
			TextDirectoryLoader loader = new TextDirectoryLoader();
			loader.setDirectory(new File("//Users//ThanhNT//Desktop//trainData//trainSubAndObj"));
			loader.setCharSet("UTF-8");
			Instances trainSubAndObj = loader.getDataSet();
			Filter filter = new SMARTStringToWordsFilter();
			Tokenizer tokenizer = new NGramTokenizer();
			((SMARTStringToWordsFilter) filter).setTokenizer(tokenizer);
			((SMARTStringToWordsFilter) filter).setOutputWordCounts(true);
			((SMARTStringToWordsFilter) filter).setTransform(WeightingScheme.ADTSN);
			filter.setInputFormat(trainSubAndObj);
			trainSubAndObj = Filter.useFilter(trainSubAndObj, filter);
			
			Class theClass  = Class.forName(algname);
			Classifier classifier = (Classifier)theClass.newInstance();
			classifier.buildClassifier(trainSubAndObj);

			loader.setDirectory(new File(folderPath));
			loader.setCharSet("UTF-8");
			Instances beginTest = loader.getDataSet();
			Instances test = new Instances(beginTest);
//			System.out.println("-----------" + test.instance(2).classIndex());
			Instances testedSubAndObj = Filter.useFilter(test, filter);
			
			System.out.println("+++++++++++++++" + test.instance(2).classIndex());

			int i = 0;
			test.clear();
			
			for (; i < testedSubAndObj.size(); ++i) {
				int clazz = (int) classifier.classifyInstance(testedSubAndObj.get(i));
				String className = testedSubAndObj.attribute(testedSubAndObj.classIndex()).value(clazz);
				result.add(className);
				if(className.equalsIgnoreCase("Subjective")){
					test.add(beginTest.get(i));
				}
				listSentences.add(beginTest.instance(i).stringValue(0));

			}
			
			
			loader.setDirectory(new File("//Users//ThanhNT//Desktop//trainData//trainPosAndNeg"));
			loader.setCharSet("UTF-8");
			Instances trainPosAndNeg = loader.getDataSet();
//			removeIncorrect(trainPosAndNeg);

			filter = new SMARTStringToWordsFilter();
			((SMARTStringToWordsFilter) filter).setTokenizer(tokenizer);
			((SMARTStringToWordsFilter) filter).setOutputWordCounts(true);
			((SMARTStringToWordsFilter) filter).setTransform(WeightingScheme.ADTSN);

			filter.setInputFormat(trainPosAndNeg);
			trainPosAndNeg = Filter.useFilter(trainPosAndNeg, filter);
			classifier.buildClassifier(trainPosAndNeg);

			Instances testedPosAndNeg = Filter.useFilter(test, filter);
			int k = 0;
			for (int j = 0; j < testedPosAndNeg.size(); j++) {
				int clazz = (int) classifier.classifyInstance(testedPosAndNeg.get(j));
				String className = testedPosAndNeg.attribute(testedPosAndNeg.classIndex()).value(clazz);
				boolean notAdded = true;
				do{
					if(result.get(k).equalsIgnoreCase("Subjective")){
						result.set(k, className);
						notAdded = false;
					}
					k++;
				}while(notAdded);
			}
			
			for(int l = 1; l < result.size(); ++l){
				System.err.println(result.get(l));
//				System.out.println(test.instance(l).stringValue(0));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	
	public static void removeIncorrect(Instances instances){
		
		for(Instance ints: instances){
			boolean valid = false;
			int numAtts = instances.numAttributes();
			for(int i = 0; i<numAtts; ++i) {
				if(i == ints.classIndex()) continue;
				double val = ints.value(i);
				if(!ints.isMissing(i) && val != 0) {
//					System.out.println("remove :" + ints.stringValue(0));
					valid = true;
					break;
				}
			}
			if(!valid) {
				instances.remove(ints);
				System.out.println("fsshsd");
			}
		}
	}

	public static boolean saveArff(Instances instances, File output) {
		try {
			ArffSaver saver = new ArffSaver();
			saver.setInstances(instances);
			saver.setFile(output);
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
