package weka;



import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import utils.StringUtils;
import weka.classifiers.Classifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.tokenizers.NGramTokenizer;

public class WekaClassifier implements ClassifierWrapper {

	public static final String CLASS_ATTR = "@@class@@";
	public static final String CLASSPATH_SEPARATOR = File.separator;
	private String classifierName;
	private File trainDirectory;
	private TextDirectoryWithSubdirectoryLoader loader;
	private Instances trainData;
	private Set<String> classes;
	
	public WekaClassifier(String classifierName, File trainDirectory) {
		this.trainDirectory = trainDirectory;
		this.classifierName = classifierName;
		loader = new TextDirectoryWithSubdirectoryLoader();
	}

	@Override
	public Set<String> getClasses() {
		if(classes != null) return classes;
		classes = new HashSet<String>();
		Instances trainData = loadTrainDirectory();
		for(Instance instance: trainData) {
			String path = instance.stringValue(1);
			path = path.substring((trainDirectory.getPath() + CLASSPATH_SEPARATOR).length());
			path = path.substring(0, path.lastIndexOf(CLASSPATH_SEPARATOR));
			classes.add(path);
		}
		return classes;
	};
	
	private Instances loadTrainDirectory() {
		if(trainData != null) return trainData;
		try {
			loader.setDirectory(trainDirectory);
			loader.setCharSet("UTF-8");
			loader.setOutputFilename(true);
			trainData = loader.getDataSet();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return trainData;
	}
	
	private Instances getTrainData(String parent) throws Exception {
		trainData = loadTrainDirectory();
		// 1. set up attributes
		FastVector attrs = new FastVector();
		FastVector classes = new FastVector();
		attrs.add(new Attribute("train", (FastVector)null));
		
		for(Instance instance: trainData) {
	    	String path = instance.stringValue(1);
	    	path = path.substring((trainDirectory + CLASSPATH_SEPARATOR).length());
	    	if(path.startsWith(parent) || parent.equals("")) {
	    		int start = (parent == "") ? 0 : (parent + CLASSPATH_SEPARATOR).length();
	    		if(start < 0) System.out.println("<0");
				int end = path.indexOf(CLASSPATH_SEPARATOR, start);
		    	if(end == -1) continue;
		    	path = path.substring(start, end);
				if(!classes.contains(path)) {
					classes.add(path);
				}
	    	}
		}
		if(classes.size() == 0) return null;
		attrs.add(new Attribute(CLASS_ATTR, classes));
		// 2. create Instances object
	    Instances train = new Instances(CLASS_ATTR, attrs, 0);
	    
	    train.setClassIndex(1);
	    // 3. fill with data
	    for(Instance instance: trainData) {
	    	String path = instance.stringValue(1);
	    	path = path.substring((trainDirectory + CLASSPATH_SEPARATOR).length());
	    	if(path.startsWith(parent) || parent.equals("")) {
	    		int start = (parent == "") ? 0 : (parent + CLASSPATH_SEPARATOR).length();
				int end = path.indexOf(CLASSPATH_SEPARATOR, start);
				if(end == -1) {
					continue;
				}
		    	path = path.substring(start, end);
		    	//System.out.println(path);
				String txt = instance.stringValue(0);
				double[] vals = new double[2];
				vals[0] = train.attribute(0).addStringValue(txt);
				vals[1] = classes.indexOf(path);
				Instance newInstance = new DenseInstance(1, vals);
				newInstance.setDataset(train);
				train.add(newInstance);
			}
		}

		return train;
	}
	
	@Override
	public ArrayList<String> classify(List<String> txts) {
		ArrayList<String> result = new ArrayList<String>();
		for(int i = 0; i<txts.size();++i) {
			result.add("");
		}
		// whatever would be fine
		FastVector classVector = new FastVector();
		final String TEST = "test";
		classVector.addElement(TEST);
		// 1. set up attributes
		FastVector attrs = new FastVector();
		attrs.add(new Attribute(TEST, (FastVector)null));
		attrs.add(new Attribute(CLASS_ATTR, classVector));
		// 2. create Instances object
	    Instances test = new Instances(CLASS_ATTR, attrs, 0);
	    test.setClassIndex(1);
	    // 3. fill with data
		for(String txt: txts) {
			double[] vals = new double[2];
			vals[0] = test.attribute(0).addStringValue(txt);
			vals[1] = 0;
			Instance instance = new DenseInstance(1, vals);
			instance.setDataset(test);
			test.add(instance);
		}

		Map<String, FilteredClassifier> classifiers = new HashMap<String, FilteredClassifier>();
		for(int i = 0; i<test.numInstances(); ++i) {
			try {
				while(true) {
					Instances train;
					String clazz = result.get(i);
					if(getClasses().contains(clazz)) break;
					FilteredClassifier classifier = classifiers.get(clazz);
					if(classifier == null) {
						train = getTrainData(clazz);
						classifier = new FilteredClassifier();
						SMARTStringToWordsFilter filter = new SMARTStringToWordsFilter();
						classifier.setFilter(filter);
						Class cclass = Class.forName(classifierName);
						classifier.setClassifier((Classifier)cclass.newInstance());
						filter.setTokenizer(new NGramTokenizer());
						filter.setOutputWordCounts(true);
						filter.setTransform(SMARTStringToWordsFilter.WeightingScheme.ADTSN);
						filter.setInputFormat(train);
						classifier.buildClassifier(train);
						
						classifiers.put(clazz, classifier);
					}
					train = classifier.getFilter().getOutputFormat();
					
					int classIndex = (int) classifier.classifyInstance(test.get(i));
					String classifyAs = train.classAttribute().value(classIndex);
					if(!clazz.equals("")) classifyAs = clazz + CLASSPATH_SEPARATOR + classifyAs;
					result.set(i, classifyAs);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
//	public static void main(String[] args) {
//		WekaClassifier classifier = new WekaClassifier("weka.classifiers.functions.SMO", new File("F:/trainData"));
//		String[] test = {
//			"xin chào",
//			"xin chào",
//			"xin chào",
//			"xin chào",
//			"xin chào"};
//		String[] result =  classifier.classify(test);
//		System.out.println(result[0]);
//	}
}