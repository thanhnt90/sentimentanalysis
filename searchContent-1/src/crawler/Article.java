package crawler;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Article {
	long id;
	private String title;
	private String content;
	private Date date;
	private int likes;
	private List<Post> comments = new ArrayList<Post>();
	private String lead;
	
	public int getLikes() {
		return likes;
	}
	public void setLikes(int likes) {
		this.likes = likes;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public String getFormatedContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public List<Post> getComments() {
		return comments;
	}
	public void setComments(List<Post> comments) {
		this.comments = comments;
	}
	public String getLead() {
		return lead;
	}
	public void setLead(String lead) {
		this.lead = lead;
	}
}