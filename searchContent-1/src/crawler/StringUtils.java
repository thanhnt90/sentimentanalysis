package crawler;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;

public abstract class StringUtils {
	public static String fromUrl(String urlString) {
		try {
			URL url = new URL(urlString);
			return fromInputStream(url.openStream());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static int BUFFER_SIZE = 8192;
	public static String fromInputStream(InputStream inputStream) {
		try {
			BufferedInputStream in = new BufferedInputStream(inputStream);
			StringBuilder stringBuilder = new StringBuilder();
			byte[] buffer = new byte[BUFFER_SIZE];
			int read;
			while ((read = in.read(buffer)) != -1) {
				stringBuilder.append(new String(buffer, 0, read, "utf-8"));
			}
			inputStream.close();
			return stringBuilder.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}


	public static String fromFile(File file) {
		try {
			FileInputStream inputStream = new FileInputStream(file);
			return fromInputStream(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public enum ReturnCode {
		SAVED, UPDATED, FAILED
	}
	
	public static ReturnCode toFile(String content, File file) {
		Writer writer = null;
		ReturnCode succeed = ReturnCode.UPDATED;
		try {
			File folder = file.getParentFile();
			if(!folder.exists()) {
				folder.mkdirs();
			}
			writer = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(file, false), "UTF-8"));
			writer.write(content);
		} catch (Exception e) {
			e.printStackTrace();
			return ReturnCode.FAILED;
		} finally {
			try {
				writer.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return succeed;
	}
}
