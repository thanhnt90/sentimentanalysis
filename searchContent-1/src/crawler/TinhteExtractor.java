package crawler;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;

import stograge.FileStograge;

class TinhteExtractor implements ContentExtractor{
	private final static FileStograge FILE_STOGRAGE = new FileStograge(
			"/Users/ThanhNT/Desktop/tinhteData");
	private static final String BASE_URL = "http://tinhte.vn";
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"hh:mm|dd/MM/yyyy");
	private static final Pattern ID_PATTERN = Pattern
			.compile("\\d+");
	@Override
	public Article process(String url) throws Exception {
//		Article article = new Article();
//		HtmlCleaner cleaner = new HtmlCleaner();
//		TagNode root = cleaner.clean(new URL(url));
//		root = root.findElementByName("body", false);
//		TagNode node = root.findElementByAttValue("class", "titleBar", true, true);
//		article.setTitle(node.findElementByName("h1", false).getText().toString());
//		node = root.findElementByAttValue("class", "messageText ugc baseHtml", true, true);
//		for(TagNode tagNode: node.getElementsByName("script", true)) {
//			tagNode.removeFromTree();
//		}
//		article.setContent(node.getText().toString());
////		System.out.println(node.getText().toString());
//		
//		TagNode timeNode = root.findElementByAttValue("class", "message", true, true)
//				.getElementsByName("abbr", true)[0];
//		System.out.println("date: " + timeNode.getAttributeByName("data-datestring")
//				+ "time : " + timeNode.getAttributeByName("data-timestring"));
//		return null;
//		
		Article article = new Article();
		
		long id = 0;
		Matcher makeMatch = ID_PATTERN.matcher(url);
		makeMatch.find();
		String inputInt = makeMatch.group();
//		System.out.println(inputInt);
		article.setId(Long.parseLong(inputInt));
		
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode root = cleaner.clean(new URL(url));
		TagNode node = root.findElementByAttValue("class", "titleBar", true, true);
		article.setTitle(node.findElementByName("h1", false).getText().toString());
		
		TagNode listPostNode = root.findElementByAttValue("class", "messageList", true, true);
		TagNode[] listMsg = listPostNode.getElementsByName("li", true);
		node = listMsg[0].findElementByAttValue("class", "messageText ugc baseHtml", true, true);
		for(TagNode tagNode: node.getElementsByName("script", true)) {
			tagNode.removeFromTree();
		}
		article.setContent(node.getText().toString().trim());
		System.out.println(article.getId());
		node = listMsg[0].findElementByAttValue("class", "DateTime", true, true);
		
		Map<String, String> map = node.getAttributes();
		for(String str:map.keySet()){
			System.out.println("key : " + str + " value: " + map.get(str));
		}
		
		String dateTime = node.getAttributeByName("title") == null?
				node.getText().toString():node.getAttributeByName("title");
		System.out.println(dateTime);
		String[] dateAndTime = dateTime.split("at");
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(dateAndTime[1].trim()).append("|");
		String[] dateStr = dateAndTime[0].trim().split("/");
		stringBuffer.append(dateStr[0].length() > 1 ? dateStr[0]:("0" + dateStr[0])).append("/");
		stringBuffer.append(dateStr[1].length() > 1 ? dateStr[1]:("0" + dateStr[1])).append("/");
		stringBuffer.append("20" + dateStr[2]);
		article.setDate(DATE_FORMAT.parse(stringBuffer.toString()));
		System.out.println(stringBuffer);
		
		article.setLead("");
		article.setLikes(0);
		getAllComments(article, url);
		
		return article;
		
	}
	
	public static void getAllComments(Article article, String url) throws Exception{
		List<Post> posts = article.getComments();
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode root = cleaner.clean(new URL(url));
		TagNode node = root.findElementByAttValue("class", "PageNav", true, true);
		int currentPage = 1;
		int lastPage = 1;
		if(node != null){
			currentPage = Integer.parseInt(node.getAttributeByName("data-page").toString());
			lastPage = Integer.parseInt(node.getAttributeByName("data-last").toString());
		}
		TagNode listPostNode = root.findElementByAttValue("class", "messageList", true, true);
		TagNode[] listMsg = listPostNode.getElementsByName("li", true);

//		System.out.println("count list: " + listMsg.length);
		for(TagNode cmtNode: listMsg){
			if(cmtNode.getAttributeByName("data-author") == null)continue;
			if(!cmtNode.getAttributeByName("class").toString().
					equalsIgnoreCase("message firstPost  staff "))
				posts.add(parseComment(cleaner, cmtNode));
		}
		if(currentPage < lastPage){
			String nextURL = BASE_URL + "/threads/" + article.getId() + "/page-" + (currentPage+1);
			getAllComments(article, nextURL);
		}	
	}
	
	private static Post parseComment(HtmlCleaner cleaner, TagNode comment)throws Exception {
		Post post = new Post();
		TagNode node = comment.findElementByAttValue("class", "messageText ugc baseHtml", true, true);
		for(TagNode tagNode: node.getElementsByName("script", true)) {
			tagNode.removeFromTree();
		}
		post.setContent(node.getText().toString().trim());
		
		node = comment.findElementByAttValue("class", "DateTime", true, true);
		
		String dateTime = node.getAttributeByName("title") == null?
				node.getText().toString():node.getAttributeByName("title");
		String[] dateAndTime = dateTime.split("at");
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(dateAndTime[1].trim()).append("|");
		String[] dateStr = dateAndTime[0].trim().split("/");
		stringBuffer.append(dateStr[0].length() > 1 ? dateStr[0]:("0" + dateStr[0])).append("/");
		stringBuffer.append(dateStr[1].length() > 1 ? dateStr[1]:("0" + dateStr[1])).append("/");
		stringBuffer.append("20" + dateStr[2]);
		post.setDate(DATE_FORMAT.parse(stringBuffer.toString()));
		return post;
	}

	public static void main(String args[]){
		TinhteExtractor extractor = new TinhteExtractor();
		try {
			Article article = extractor.process("http://www.tinhte.vn/threads/1572009/");
			FILE_STOGRAGE.saveOrUpdate(article);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
