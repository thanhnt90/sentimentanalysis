package crawler;

import java.util.regex.Pattern;

import stograge.FileStograge;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import edu.uci.ics.crawler4j.url.WebURL;

public class Crawler {
	private final static FileStograge FILE_STOGRAGE = new FileStograge(
			"//Users//ThanhNT//Desktop//tinhteData");
	private final static ThreadPool THREAD_POOL = new ThreadPool(1000);

	public static class MyCrawler extends WebCrawler {

		private final static Pattern FILTERS = Pattern
				.compile(".*(\\.(css|js|bmp|gif|jpe?g"
						+ "|png|tiff?|mid|mp2|mp3|mp4"
						+ "|wav|avi|mov|mpeg|ram|m4v|pdf"
						+ "|rm|smil|wmv|swf|wma|zip|rar|gz))$");
		private final static ContentExtractor CONTENT_EXTRACTOR = new TinhteExtractor();

		/**
		 * You should implement this function to specify whether the given url
		 * should be crawled or not (based on your crawling logic).
		 */
		@Override
		public boolean shouldVisit(WebURL url) {
			String href = url.getURL().toLowerCase();
//			return !FILTERS.matcher(href).matches()
//					&& href.startsWith("http://vnexpress.net/gl");
			return !FILTERS.matcher(href).matches()
					&& href.startsWith("http://www.tinhte.vn/threads")
					&& href.indexOf("page") < 0;

		}

		/**
		 * This function is called when a page is fetched and ready to be
		 * processed by your program.
		 */
		@Override
		public void visit(Page page) {
			final String url = page.getWebURL().getURL();
			try {
				THREAD_POOL.execute(new Runnable() {
					@Override
					public void run() {
						try {
							System.out.println(url);
							if(url.startsWith("http://www.tinhte.vn/threads")){
								Article article = CONTENT_EXTRACTOR.process(url);
								FILE_STOGRAGE.saveOrUpdate(article);
							}
						} catch (Exception e) {// Catch exception if any
							e.printStackTrace();
						}
					}
				});
			} catch (Exception e) {// Catch exception if any
				e.printStackTrace();
			}

		}
	}

	public static void main(String[] args) throws Exception {
		String crawlStorageFolder = "//Users//ThanhNT//Desktop//tinhteData//data//crawl//root";
		int numberOfCrawlers = 7;

		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);
		config.setResumableCrawling(true);
		/*
		 * Instantiate the controller for this crawl.
		 */
		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig,
				pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher,
				robotstxtServer);

		/*
		 * For each crawl, you need to add some seed urls. These are the first
		 * URLs that are fetched and then the crawler starts following links
		 * which are found in these pages
		 */

//		controller.addSeed("http://vnexpress.net/");
		controller.addSeed("http://www.tinhte.vn/");
		/*
		 * Start the crawl. This is a blocking operation, meaning that your code
		 * will reach the line after this only when crawling is finished.
		 */
		controller.start(MyCrawler.class, numberOfCrawlers);
	}
}