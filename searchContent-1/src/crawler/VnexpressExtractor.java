package crawler;

import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringEscapeUtils;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;

class VnexpressExtractor implements ContentExtractor {
	private static final String BASE_URL = "http://vnexpress.net";
	private static final Pattern ID_PATTERN = Pattern
			.compile("Lpid = \"(\\d+)\";");
	private static final String COMMENT_QUERY = BASE_URL
			+ "/service/comment/showcomment.asp?id=%s&p=%d";
	private static final DateFormat DATE_FORMAT = new SimpleDateFormat(
			"hh:mm|dd/MM/yyyy");
	private static final DateFormat DATE_FORMAT_WITHOUT_TIME = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final Pattern RECENT_MINS_PATTERN = Pattern.compile("[^\\d]*(\\d+)[^\\d]*");
	private static final Pattern RECENT_HOURS_PATTERN = Pattern.compile("[^\\d]*(\\d+)[^\\d]*(\\d+)[^\\d]*");
	
	@Override
	public Article process(String url) throws Exception {
		Article article = new Article();
		HtmlCleaner cleaner = new HtmlCleaner();
		TagNode root = cleaner.clean(new URL(url));
		root = root.findElementByName("body", false);
		TagNode node = root.findElementByAttValue("class", "Title", true, true);
		article.setTitle(node.getText().toString());
		node = root.findElementByAttValue("class", "item-time", true, true);
		StringBuffer stringBuffer = node.getText();
		node = root.findElementByAttValue("class", "item-date", true, true);
		stringBuffer.append(node.getText());
		article.setDate(DATE_FORMAT.parse(stringBuffer.toString().replace("&nbsp;", "")));
		
		node = root.findElementByAttValue("class", "item-date", true, true);
		article.setLead(node.toString());

		String content;
		stringBuffer.setLength(0);
		Object[] matches = root.evaluateXPath("/h2[@class='Normal']/node()");
		for (int i = 0; i < matches.length - 1; ++i) {
			content = ((StringBuffer)matches[i]).toString();
			stringBuffer.append(Jsoup.parse(content).text()).append("\r\n");
		}
		content = stringBuffer.toString().trim();
		article.setContent(content);

		getAllComments(article, root);
		return article;
	}

	public static void getAllComments(Article article, TagNode root)
			throws Exception {
		List<Post> posts = article.getComments();
		HtmlCleaner cleaner = new HtmlCleaner();
		String html = cleaner.getInnerHtml(root);
		Matcher matcher = ID_PATTERN.matcher(html);
		StringBuilder comments = new StringBuilder();
		if (matcher.find()) {
			String id = matcher.group(1);
			article.setId(Long.parseLong(id));
			Object[] match = root.evaluateXPath("//a[@class='Paging Red']");
			for (int i = 0; i <= match.length; ++i) {
				html = StringUtils.fromUrl(String.format(COMMENT_QUERY, id,
						i + 1));
				comments.append(html);
			}
		}

		html = StringEscapeUtils.unescapeHtml4(html);
		root = cleaner.clean(html);
		Object match[] = root.evaluateXPath("//div/div");
		for(Object obj: match) {
			posts.add(parseComment(cleaner, (TagNode) obj));
		}
	}

	private static Post parseComment(HtmlCleaner cleaner, TagNode comment) {
		Post post = new Post();
		TagNode node = comment.findElementByAttValue("class", "Title", true, true);
		post.setTitle(node.getText().toString());

		StringBuffer stringBuffer = new StringBuffer();
		String text = null;
		try {
			Object[] matches = comment.evaluateXPath("/*[@class='Normal']/text()");
			for (int i = 0; i < matches.length - 1; ++i) {
				text = ((StringBuffer)matches[i]).toString();
				stringBuffer.append(text).append("\r\n");
			}
			post.setContent(stringBuffer.toString().trim());
		
			node = comment.findElementByAttValue("class", "CommDate", true, true);
			text = node.getText().toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Date date = null;
		try {
			date = DATE_FORMAT_WITHOUT_TIME.parse(text);
		} catch (ParseException e) {
			Calendar time = Calendar.getInstance();
			Matcher matcher = RECENT_HOURS_PATTERN.matcher(text);
			byte mins;
			if(matcher.matches()) {
				byte hours = Byte.parseByte(matcher.group(1));
				mins = Byte.parseByte(matcher.group(2));
				time.add(Calendar.HOUR, -1 * hours);
			} else {
				matcher = RECENT_MINS_PATTERN.matcher(text);
				mins = Byte.parseByte(matcher.group(1));
			}
			time.add(Calendar.MINUTE, -1 * mins);
			date = time.getTime();
		} catch (Exception e) {
			e.printStackTrace();
		}
		post.setDate(date);
		return post;
	}
}