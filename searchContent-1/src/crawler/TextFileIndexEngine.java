package crawler;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.apache.lucene.util.Version;

import stograge.FileStograge;


/**
 * This terminal application creates an Apache Lucene index in a folder and adds files into this index
 * based on the input of the user.
 */
public class TextFileIndexEngine {
	public static final String FILE_SEPARATOR = System.getProperty("file.separator");

	public static final String FILES_TO_INDEX_DIRECTORY = "//Users//ThanhNT//Desktop//crawler//crawler";
	public static final String INDEX_DIRECTORY = "//Users//ThanhNT//Desktop//index";

	public static final String FIELD_PATH = "path";
	public static final String FIELD_CONTENT = "content";
	public static final String FIELD_COMMENT = "comment";
	public static final String FIELD_DATE = "date";
	public static final String FIELD_LEAD = "lead";
	public static final String FIELD_TITLE = "title";
//	public static final VietTokenizer TOKENIZER = new VietTokenizer();

	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyMMdd");

	
	public static final FileStograge FILE_STOGRAGE = new FileStograge(FILES_TO_INDEX_DIRECTORY);

	private IndexWriter writer;
	private ArrayList<File> queue = new ArrayList<File>();

	public static void main(String[] args) throws IOException {
		File indexDirectory = new File(INDEX_DIRECTORY);
		if(indexDirectory.isDirectory())
		for(File f: indexDirectory.listFiles()){
			f.delete();
		}
		System.out.println("The path where the index will be created: " + INDEX_DIRECTORY);
		TextFileIndexEngine indexer = null;
		try {
			indexer = new TextFileIndexEngine(INDEX_DIRECTORY);
		} catch (Exception ex) {
			System.out.println("Cannot create index..." + ex.getMessage());
			System.exit(-1);
		}

		
		
		System.out.println("The path where the index will be created: " + FILES_TO_INDEX_DIRECTORY);

		indexer.createIndex();
		//===================================================
		//after adding, we always have to call the
		//closeIndex, otherwise the index is not created    
		//===================================================
		indexer.closeIndex();	
	}

	/**
	 * Constructor
	 * @param indexDir the name of the folder in which the index should be created
	 * @throws java.io.IOException
	 */
	TextFileIndexEngine(String indexDir) throws IOException {
		// the boolean true parameter means to create a new index everytime, 
		// potentially overwriting any existing files there.
		
		FSDirectory dir = FSDirectory.open(new File(indexDir));

		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_35);

		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_35, analyzer);

		writer = new IndexWriter(dir, config);
	}

	public void deleteDocumentsFromIndexUsingTerm(Term term) throws IOException, ParseException {

		System.out.println("Deleting documents with field '" + term.field() + "' with text '" + term.text() + "'");
		FSDirectory directory = FSDirectory.open(new File(INDEX_DIRECTORY));

		IndexReader indexReader = IndexReader.open(directory);
		indexReader.deleteDocuments(term);
		indexReader.close();

	}

	/**
	 * Close the index.
	 * @throws java.io.IOException
	 */
	public void closeIndex() throws IOException {
		writer.close();
	}
	
	public void createIndex() throws CorruptIndexException, LockObtainFailedException, IOException {
		int originalNumDocs = writer.numDocs();
		for(String fileName: FILE_STOGRAGE.listThreads()) {
			try {
				Document doc = new Document();
				if(fileName.equals("1011021000482118") || fileName.equals("1201311002249787")) continue;
				Article article = FILE_STOGRAGE.articleFromDirectory(fileName);
				
				doc.add(new Field(FIELD_TITLE, article.getTitle(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
				doc.add(new Field(FIELD_LEAD, article.getLead(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
				doc.add(new Field(FIELD_CONTENT, article.getContent(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
				doc.add(new Field(FIELD_DATE, fileName, Field.Store.YES, Field.Index.ANALYZED));
				doc.add(new Field(FIELD_PATH, FILES_TO_INDEX_DIRECTORY + FILE_SEPARATOR + fileName + FILE_SEPARATOR + fileName, Field.Store.YES, Field.Index.NOT_ANALYZED));

				for(Post p: article.getComments()){
					Document docComment = new Document();
					docComment.add(new Field(FIELD_TITLE, p.getTitle(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
					docComment.add(new Field(FIELD_DATE, fileName, Field.Store.YES, Field.Index.ANALYZED));
					docComment.add(new Field(FIELD_PATH, FILES_TO_INDEX_DIRECTORY + FILE_SEPARATOR +  fileName + FILE_SEPARATOR +  p.getFileName(), Field.Store.YES, Field.Index.NOT_ANALYZED));
					String s;
					docComment.add(new Field(FIELD_COMMENT, p.getContent(), Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES));
					writer.addDocument(docComment);
				}
				
				writer.addDocument(doc);
				System.out.println("Added: " + fileName + "\n" + article.getComments().size() + " comments");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Could not add file " + fileName);
			}
		}

		int newNumDocs = writer.numDocs();
		System.out.println("");
		System.out.println("************************");
		System.out.println((newNumDocs - originalNumDocs) + " documents added.");
		System.out.println("************************");

		queue.clear();
	}
	
//	public static String tokenize(String string) {
//		String[] tokens = TOKENIZER.tokenize(string);
//		StringBuilder stringBuilder = new StringBuilder();
//		for(String token: tokens) {
//			stringBuilder.append(token);
//		}
//		return stringBuilder.toString();
//	}	
}